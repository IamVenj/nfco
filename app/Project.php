<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;

class Project extends Model
{
    protected $fillable = ['title', 'description', 'public_id', 'version'];

    public function createProject($title, $description, $imageName)
    {
        if(!is_null($imageName))
        {
            $_image = new Image();
        	$uploadResult = $_image->addImage('Project', $imageName);
        	
            $this::create([

            	'title' => $title,
            	'description' => $description,
            	'public_id' => $uploadResult['public_id'],
            	'version' => $uploadResult['version']

            ]);
        }
        else
        {
            $this::create([

                'title' => $title,
                'description' => $description

            ]);
        }
    }

    public function updateProjectText($id, $title, $description)
    {
    	$Project = $this::find($id);

    	$Project->title = $title;
    	$Project->description = $description;

    	$Project->save();
    }

    public function updateProjectImage($id, $imageName)
    {
        $_image = new Image();
    	$_image->updateImage($id, 'Projects', $imageName, 'App\Project');
    }

    public function destroyProject($id)
    {
        $_image = new Image();
        $_image->deletePreviousImage($id, 'App\Project');
        $this::find($id)->delete();
    }
}
