<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\image;

class HomeCarousel extends Model
{
    protected $fillable = ['title', 'mini_title', 'public_id', 'version', 'description'];

    public function createCarousel($title, $mini_title, $imageName, $slug)
    {
        $_image = new Image();

    	$uploadResult = $_image->addImage('Events', $imageName);
    	$public_id = $uploadResult['public_id'];
        $version = $uploadResult['version'];

    	$this::create([

    		'title' => $title,
    		'mini_title' => $mini_title,
    		'public_id' => $public_id,
    		'version' => $version,
    		'description' => $slug

    	]);
    }

    public function updateCarouselText($id, $title, $mini_title, $slug)
    {
    	$carousel = $this::find($id);

    	$carousel->title = $title;
    	$carousel->mini_title = $mini_title;
    	$carousel->description = $slug;

    	$carousel->save();
    }

    public function updateCarouselImage($id, $imageName)
    {
        $_image = new Image();
    	$_image->updateImage($id, 'Carousel', $imageName, 'App\HomeCarousel');
    }

    public function destroyCarousel($id)
    {
        $_image = new Image();
    	$_image->deletePreviousImage($id, 'App\HomeCarousel');
    	$this::find($id)->delete();
    }
}
