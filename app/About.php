<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = ['mission', 'vision', 'objective', 'motto', 'who_are_we'];

    public function updateAbout($mission, $objective, $vision, $motto, $who_are_we)
    {
    	$about = $this::first();

    	$about->mission = $mission;
    	$about->vision = $vision;
    	$about->objective = $objective;
        $about->motto = $motto;
    	$about->who_are_we = $who_are_we;

    	$about->save();
    }
}
