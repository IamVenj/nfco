<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $fillable = ['amount', 'firstname', 'lastname', 'email', 'phone_number', 'address1', 'address2'];

    public function makePromise($amount, $firstname, $lastname, $email, $phone, $address1, $address2)
    {
    	$this::create([

            'amount' => $amount,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'phone_number' => $phone,
            'address1' => $address1,
            'address2' => $address2

        ]);
    }

    // public function checkCount($firstname, $lastname, $email, $phone)
    // {
    //     $count = $this::where('firstname', $firstname)->where('lastname', $lastname)
    // }
}
