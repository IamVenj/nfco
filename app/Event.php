<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\image;

class Event extends Model
{
    protected $fillable = ['public_id', 'version', 'date_from', 'date_to', 'location', 'event_name', 'event_slug', 'time'];

    protected $dates = ['date_from', 'date_to'];

    public function createEvent($date_from, $date_to, $location, $event_name, $event_slug, $time, $imageName)
    {
        $public_id = null;
        $version = null;
        
        if(!is_null($imageName))
        {
            $_image = new image();
        	$uploadResult = $_image->addImage('Events', $imageName);
        	$public_id = $uploadResult['public_id'];
            $version = $uploadResult['version'];
        }

    	$this::create([

    		'event_name' => $event_name,
    		'event_slug' => $event_slug,
    		'time' => $time,
    		'location' => $location,
    		'date_from' => $date_from,
    		'date_to' => $date_to,
    		'public_id' => $public_id,
    		'version' => $version

    	]);
    }

    public function updateEvent($id, $date_from, $date_to, $location, $event_name, $event_slug, $time)
    {
    	$event = $this::find($id);

    	$event->date_from = $date_from;
    	$event->date_to = $date_to;
    	$event->location = $location;
    	$event->event_name = $event_name;
    	$event->event_slug = $event_slug;
    	$event->time = $time;

    	$event->save();
    }

    public function updateEventImage($id, $imageName)
    {
        $_image = new image();
        $model = 'App\Event';
    	$_image->updateImage($id, 'Events', $imageName, $model);
    }

    public function destroyEvent($id)
    {
        $_image = new image();
    	$_image->deletePreviousImage($id, 'App\Event');
    	$this::find($id)->delete();
    }
}
