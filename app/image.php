<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class image extends Model
{
    public function deletePreviousImage($id, $model)
    {
        try {
        	$definedModel = $model::find($id);
        	\Cloudder::destroy($definedModel->public_id);
        } catch (Exception $e) {
            return back()->withErrors("Sorry! an Error has occured! Please try again! ".$e);
        }
    }

    public function addImage($folderName, $imageName)
    {
        try {
        	$imageResult = \Cloudinary\Uploader::upload($imageName, array("folder"=>$folderName));
        	return $imageResult;
        } catch (Exception $e) {
            return back()->withErrors("Sorry! an Error has occured! Please try again! ".$e);
        }
    }

    public function updateImage($id, $folderName, $imageName, $model)
    {
        $definedModel = $model::find($id);

        if(!is_null($definedModel->public_id))
            $this->deletePreviousImage($id, $model);
        
        $uploadResult = $this->addImage($folderName, $imageName);

        $definedModel->public_id = $uploadResult['public_id'];
        $definedModel->version = $uploadResult['version'];

        $definedModel->save();
    }
}
