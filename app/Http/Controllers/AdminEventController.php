<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;

use Carbon\Carbon;

class AdminEventController extends Controller
{
    private $_event, $_category;

    public function __construct()
    {
        $this->_event = new Event();
        $this->middleware('auth');
    }

    public function index()
    {

        $events = $this->_event::latest()->paginate(8);

    	return view('post-login.pages.event.index', compact('events'));

    }

    public function create()
    {
    	return view('post-login.pages.event.create');
    }

    public function store(Request $request)
    {
        $this::validate(request(), [

            'name' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'time' => 'required',
            'slug' => 'required',
            'location' => 'required',

        ]);

        $name = $request->name;
        $date_from = Carbon::createFromFormat('d/m/Y', $request->date_from)->format('Y-m-d');
        $date_to = Carbon::createFromFormat('d/m/Y', $request->date_to)->format('Y-m-d');
        $time = $request->time;

        $image = $request->file('image');
        $image_name = $image->getRealPath();    
        
        $slug = $request->slug;
        $location = $request->location;

        if(!is_null($request->image))
        {
            $this::validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);
        }

        $this->_event->createEvent($date_from, $date_to, $location, $name, $slug, $time, $image_name);

        return redirect(route('event.index'))->with('success', 'Event is successfully created');

    }


    public function update($id)
    {
        $this::validate(request(), [

            'name' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'time' => 'required',
            'slug' => 'required',
            'location' => 'required',

        ]);

        $name = request('name');
        $date_from = Carbon::createFromFormat('d/m/Y', request('date_from'))->format('Y-m-d');
        $date_to = Carbon::createFromFormat('d/m/Y', request('date_to'))->format('Y-m-d');
        $time = request('time');
        $slug = request('slug');
        $location = request('location');

        $this->_event->updateEvent($id, $date_from, $date_to, $location, $name, $slug, $time);

        return back()->with('success', 'Event is successfully updated');
    }

    public function updateImage($id, Request $request)
    {
        $this::validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);

        $image = $request->file('image');
        $image_name = $image->getRealPath();

        $this->_event->updateEventImage($id, $image_name);

        return back()->with('success', 'Event image is successfully updated');
    }

    public function destroy($id)
    {
        $this->_event->destroyEvent($id);
        return back()->with('success', 'Event is successfully deleted!');
    }
}
