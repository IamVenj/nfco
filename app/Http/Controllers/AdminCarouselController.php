<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomeCarousel;

class AdminCarouselController extends Controller
{
    private $_carousel;

	public function __construct()
	{
        $this->_carousel = new HomeCarousel();
		$this->middleware('auth');
	}

    public function index()
    {
        $carousels = $this->_carousel::latest()->paginate(9);
    	return view('post-login.pages.carousel.index', compact('carousels'));
    }

    public function create()
    {
    	return view('post-login.pages.carousel.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [

            'title' => 'required',
            'slug' => 'required',
            'image' => 'required|mimes:png,jpeg|max:5000'

        ]);

        $title = $request->title;
        $mini_title = $request->mini_title;
        $image = $request->file('image');
        $imageName = $image->getRealPath();
        $slug = $request->slug;

        $this->_carousel->createCarousel($title, $mini_title, $imageName, $slug);

        return redirect(route('carousel.index'))->with('success', 'Carousel is successfully created!');
    }

    public function update($id, Request $request)
    {
        $this->validate(request(), [

            'title' => 'required',
            'slug' => 'required'

        ]);

        $title = $request->title;
        $mini_title = $request->mini_title;
        $slug = $request->slug;

        $this->_carousel->updateCarouselText($id, $title, $mini_title, $slug);

        return back()->with('success', 'Carousel is successfully updated!');
    }

    public function updateImage($id, Request $request)
    {
        $this->validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);

        $image = $request->file('image');
        $imageName = $image->getRealPath();

        $this->_carousel->updateCarouselImage($id, $imageName);

        return back()->with('success', 'Carousel image is successfully updated!');
    }

    public function destroy($id)
    {
        $this->_carousel->destroyCarousel($id);
        return back()->with('success', 'Carousel is successfully deleted!');
    }

}
