<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    private $_news;

    public function __construct()
	{
        $this->_news = new News();
	}

    public function index()
    {
        $newz = $this->_news::latest()->paginate(12);
    	return view('pre-login.pages.news', compact('newz'));
    }

    public function show($news_name, $id)
    {
        $news = $this->_news::find($id);
    	return view('pre-login.pages.single-news', compact('news'));
    }
}
