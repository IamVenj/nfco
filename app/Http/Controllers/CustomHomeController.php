<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;

class CustomHomeController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
	   return view('post-login.pages.custom-pages.custom-home.create');
    }

    public function store(Request $request)
    {
        $mainImage = $request->mainImage;
        $coverImage = $request->coverImage;

        if(!is_null($mainImage) && !is_null($coverImage))
        {
        	$this->validate(request(), [
                'mainImage' => 'required|mimes:jpeg|max:5000',
        		'coverImage' => 'required|mimes:jpeg|max:5000'
        	]);
            $cover_update = 'cover-img.'.$coverImage->getClientOriginalExtension();
            $main_update = 'main-img.'.$mainImage->getClientOriginalExtension();
            Storage::delete(['public/uploads/custom-pages/all/'.$cover_update, 'public/uploads/custom-pages/all/'.$main_update]);
            Storage::putFileAs('public/uploads/custom-pages/all', $mainImage, $main_update);
            Storage::putFileAs('public/uploads/custom-pages/all', $coverImage, $cover_update);
            
            return back()->with('success', 'Both Images are successfully updated!');
        }
        elseif(!is_null($mainImage) && is_null($coverImage))
        {
            $this->validate(request(), [
                'mainImage' => 'required|mimes:jpeg|max:5000',
            ]);
            $image_request_main = 'main-img.'.$mainImage->getClientOriginalExtension();
            Storage::delete('public/uploads/custom-pages/all/'.$image_request_main);
            Storage::putFileAs('public/uploads/custom-pages/all', $mainImage, $image_request_main);
            
            return back()->with('success', 'main Image is successfully updated!');
        }
        elseif(is_null($mainImage) && !is_null($coverImage))
        {
            $this->validate(request(), [
                'coverImage' => 'required|mimes:jpeg|max:5000'
            ]);
            $image_request_cover = 'cover-img.'.$coverImage->getClientOriginalExtension();
            Storage::delete('public/uploads/custom-pages/all/'.$image_request_cover);
        	Storage::putFileAs('public/uploads/custom-pages/all', $coverImage, $image_request_cover);
    	   
            return back()->with('success', 'Cover Image is successfully updated!');
        }

        return back()->with('success', 'Nothing to update');

    }
}
