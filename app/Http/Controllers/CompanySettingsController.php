<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CompanySetting;

class CompanySettingsController extends Controller
{

    private $settings;

	public function __construct()
	{
		$this->middleware('auth');
        $this->settings = new CompanySetting();
	}

    public function index()
    {
        $setting = $this->settings::first();
		return view('post-login.pages.settings.update', compact('setting'));
    }

     public function store()
    {
        /*
        /--------------------------------------------
        / variables sent from form ---> request() -->
        /--------------------------------------------
        */

        $location = request('location');
        $google_map_location = request('map');
        $company_name = request('company_name');
        $email = request('email');
        $phone_number = request('phone_number');
        $experience = request('experience');
        $logo = request('logo');
        $favicon = request('favicon');
        $facebook = request('facebook');
        $twitter = request('twitter');
        $google_plus = request('google_plus');
        $linked_in = request('linked_in');
        $pinterest = request('pinterest');


        /*
        /------------------------
        / validating the form
        /------------------------
        */

        if(!is_null($logo) && !is_null($favicon))
        {

            $this->validate(request(),[

                'location' => 'required',
                'email' => 'required',
                'phone_number' => 'required',
                'company_name' => 'required',
                'logo' => 'required|mimes:png,svg|max:7000',
                'favicon' => 'required|mimes:png,svg|max:7000'

            ]);

        }

        elseif(!is_null($favicon) && is_null($logo))
        {

            $this->validate(request(),[

                'location' => 'required',
                'email' => 'required',
                'phone_number' => 'required',
                'company_name' => 'required',
                'favicon' => 'required|mimes:png,svg|max:7000'

            ]);

        }

        elseif(is_null($favicon) && !is_null($logo))
        {

            $this->validate(request(),[

                'location' => 'required',
                'email' => 'required',
                'phone_number' => 'required',
                'company_name' => 'required',
                'logo' => 'required|mimes:png,svg|max:7000',

            ]);

        }

        else
        {

            $this->validate(request(),[
                'location' => 'required',
                'email' => 'required',
                'company_name' => 'required',
                'phone_number' => 'required',
            ]);

        }

        /*
        /--------------------
        / Updating Settings
        /--------------------
        */

        $this->settings->update_settings($location, $company_name, $google_map_location, $phone_number, $email, $logo, $favicon, $facebook, $twitter, $google_plus, $linked_in, $pinterest);

        return back()->with('success', 'Setting is successfully updated');
    
    }

}
