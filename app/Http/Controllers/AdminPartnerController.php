<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sponsor;

class AdminPartnerController extends Controller
{
    private $_partner;

    public function __construct()
    {
        $this->_partner = new Sponsor();
        $this->middleware('auth');
    }

    public function index()
    {
        $partners = $this->_partner::latest()->get();
		return view('post-login.pages.partners.index', compact('partners'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), ['image' => 'required|mimes:png|max:5000']);

        $webURL = $request->link;
        $image = $request->file('image');
        $imageName = $image->getRealPath();

        $this->_partner->createSponsor($webURL, $imageName);

        return back()->with('success', 'Sponsor is successfully created!');
    }

    public function destroy($id)
    {
        $this->_partner->destroySponsor($id);
        return back()->with('success', 'Sponsor is successfully deleted!');
    }

    public function updateImage($id, Request $request)
    {
        $this->validate(request(), ['image' => 'required|mimes:png|max:5000']);

        $image = $request->file('image');
        $imageName = $image->getRealPath();

        $this->_partner->updateSponsorImage($id, $imageName);
        return back()->with('success', 'Sponsor is successfully updated!');
    }

    public function update($id, Request $request)
    {
        $webURL = $request->link;
        $this->_partner->updateSponsorURL($id, $webURL);
        return back()->with('success', 'Sponsor is successfully updated!');
    }
}
