<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class AdminNewsController extends Controller
{
    private $_news;

    public function __construct()
	{
        $this->_news = new News();
		$this->middleware('auth');
	}

    public function index()
    {
        $newz = $this->_news::latest()->paginate(9);
    	return view('post-login.pages.news.index', compact('newz'));
    }

    public function create()
    {
    	return view('post-login.pages.news.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [

            'title' => 'required',
            'slug' => 'required',

        ]);

        $title = $request->title;
        $image = $request->file('image');
        $imageName = $image->getRealPath();
        $slug = $request->slug;

        if(!is_null($imageName))
        {
            $this->validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);
        }

        $this->_news->createNews($title, $slug, $imageName);

        return redirect(route('news.index'))->with('success', 'News is successfully created!');
    }

    public function update($id, Request $request)
    {
        $this->validate(request(), [

            'title' => 'required',
            'slug' => 'required'

        ]);

        $title = $request->title;
        $slug = $request->slug;

        $this->_news->updateNewsText($id, $title, $slug);

        return back()->with('success', 'News is successfully updated!');
    }

    public function updateImage($id, Request $request)
    {
        $this->validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);

        $image = $request->file('image');
        $imageName = $image->getRealPath();

        $this->_news->updateNewsImage($id, $imageName);

        return back()->with('success', 'News image is successfully updated!');
    }

    public function destroy($id)
    {
        $this->_news->destroyNews($id);
        return back()->with('success', 'News is successfully deleted!');
    }
}
