<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class AdminProjectController extends Controller
{
    private $_project;
    public function __construct()
	{
        $this->_project = new Project();
		$this->middleware('auth');
	}

    public function index()
    {
        $projects = $this->_project::latest()->paginate(9);
    	return view('post-login.pages.project.index', compact('projects'));
    }

    public function create()
    {
    	return view('post-login.pages.project.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [

            'title' => 'required',
            'slug' => 'required',

        ]);

        $title = $request->title;
        $image = $request->file('image');
        $imageName = $image->getRealPath();
        $slug = $request->slug;

        if(!is_null($imageName))
        {
            $this->validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);
        }

        $this->_project->createProject($title, $slug, $imageName);

        return redirect(route('project.index'))->with('success', 'Project is successfully created!');
    }

    public function update($id, Request $request)
    {
        $this->validate(request(), [

            'title' => 'required',
            'slug' => 'required'

        ]);

        $title = $request->title;
        $slug = $request->slug;

        $this->_project->updateProjectText($id, $title, $slug);

        return back()->with('success', 'Project is successfully updated!');
    }

    public function updateImage($id, Request $request)
    {
        $this->validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);

        $image = $request->file('image');
        $imageName = $image->getRealPath();

        $this->_project->updateProjectImage($id, $imageName);

        return back()->with('success', 'Project image is successfully updated!');
    }

    public function destroy($id)
    {
        $this->_project->destroyProject($id);
        return back()->with('success', 'Project is successfully deleted!');
    }
}
