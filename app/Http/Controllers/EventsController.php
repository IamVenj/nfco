<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventsController extends Controller
{
    private $_event;

    public function __construct()
	{
        $this->_event = new Event();
	}

    public function index()
    {
        $events = $this->_event::latest()->paginate(12);        
    	return view('pre-login.pages.events', compact('events'));
    }

    public function show($event_name, $id)
    {
        $event = $this->_event::find($id);
    	return view('pre-login.pages.single-event', compact('event'));
    }
}
