<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contact;

use App\Services;

use App\Event;

class DashboardController extends Controller
{
    private $_contact, $_services, $_event;

    public function __construct()
    {
        $this->middleware('auth');
        $this->_contact = new Contact();
        $this->_services = new Services();
        $this->_event = new Event();
    }

    public function index()
    {
        $contact = $this->_contact::all()->count();
        $service = $this->_services::all()->count();
        $event = $this->_event::all()->count();

		return view('post-login/pages/dashboard', compact('contact', 'service', 'event'));
    }
}
