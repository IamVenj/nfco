<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;

class ServicesController extends Controller
{
	private $_service;

    public function __construct()
	{
		$this->_service = new Services();
	}

    public function index()
    {
    	$services = $this->_service::latest()->get();
    	return view('pre-login.pages.services', compact('services'));
    }
}
