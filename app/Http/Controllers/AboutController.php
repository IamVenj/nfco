<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use App\CompanySetting;
use App\Sponsor;

class AboutController extends Controller
{
	private $_about, $_setting, $_sponsor;
	public function __construct()
	{
		$this->_about = new About;
		$this->_setting = new CompanySetting;
        $this->_sponsor = new Sponsor();
	}

    public function index()
    {
    	$about = $this->_about::first();
    	$setting = $this->_setting::first();
        $sponsors = $this->_sponsor::latest()->get();

    	return view('pre-login.pages.about', compact('about', 'setting', 'sponsors'));
    }
}
