<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donation;

class DonationController extends Controller
{
	private $_donation;

	public function __construct() {
		$this->_donation = new Donation();
        $this->middleware('auth')->except('store');
	}

    public function index()
    {
        $donations = $this->_donation::all();
        return view('post-login.pages.donations.index', compact('donations'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [

    		'amount' => 'required',
    		'firstname' => 'required',
    		'lastname' => 'required',
    		'email' => 'required',
    		'phone' => 'required',
    		'address1' => 'required',

    	]);

    	$amount = $request->amount;
    	$firstname = $request->firstname;
    	$lastname = $request->lastname;
    	$email = $request->email;
    	$phone = $request->phone;
    	$address1 = $request->address1;
    	$address2 = $request->address2;

    	$this->_donation->makePromise($amount, $firstname, $lastname, $email, $phone, $address1, $address2);

    	return response()->json(['status' => 'success', 'message' => 'Thank you '.$firstname.'! you have promised us '.$amount]);
    }
}
