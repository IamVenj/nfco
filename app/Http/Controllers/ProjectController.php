<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectController extends Controller
{
    private $_project;

    public function __construct()
	{
        $this->_project = new Project();
	}

    public function index()
    {
        $projects = $this->_project::latest()->paginate(12);
    	return view('pre-login.pages.projects', compact('projects'));
    }

    public function show($project_name, $id)
    {
        $project = $this->_project::find($id);
    	return view('pre-login.pages.single-project', compact('project'));
    }
}
