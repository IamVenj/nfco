<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class AdminAccountController extends Controller
{
	public function __constrct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
		return view('post-login.pages.accounts.update');
    }

    public function store()
    {
        $this->validate(request(), ['email' => 'required|email']);

        $email = request('email');
        $password = request('password');

        if(!is_null($password))
        {
            $this->validate(request(), ['password' => 'required|confirmed']);
        }

        $_u_s_e_r_ = new User();

        $_u_s_e_r_->_update_account_($email, $password, auth()->user()->id);

        return back()->with('success', 'Account is successfully updated!');
    }
}
