<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;

class AdminAboutController extends Controller
{
	private $_about;

    public function __construct()
    {
    	$this->_about = new About();
    	$this->middleware('auth');
    }

    public function index()
    {
    	$about = $this->_about::first();
    	return view('post-login.pages.custom-pages.custom-about.create', compact('about'));
    }

    public function update(Request $request)
    {
    	$this->validate(request(), [

    		'mission' => 'required',
    		'objective' => 'required',
    		'vision' => 'required',
    		'who_are_we' => 'required'

    	]);

    	$mission = $request->mission;
    	$objective = $request->objective;
        $vision = $request->vision;
    	$who_are_we = $request->who_are_we;
    	$motto = $request->motto;

    	$this->_about->updateAbout($mission, $objective, $vision, $motto, $who_are_we);

    	return back()->with('success', 'About is successfully updated!');
    }
}
