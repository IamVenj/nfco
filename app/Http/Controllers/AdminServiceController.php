<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services;

class AdminServiceController extends Controller
{
    private $_service;

    public function __construct()
    {
        $this->_service = new Services();
        $this->middleware('auth');
    }

    public function index()
    {
        $services = $this->_service::latest()->paginate(9);
		return view('post-login.pages.service.index', compact('services'));   
    }

    public function create()
    {
		return view('post-login.pages.service.create');
    }

    public function store()
    {
        $title = request('title');
        $slug = request('slug');

        $this->validate(request(),[

            'title' => 'required',
            'slug' => 'required'

        ]);

        $this->_service->createService($title, $slug);
        return redirect(route('service.index'))->with('success', 'Service is successfully created');

    }

    public function update($id)
    {

        $title = request('title');
        $slug = request('slug');

        $this->validate(request(), [

            'title' => 'required',
            'slug' => 'required'

        ]);

        $this->_service->updateService($id, $title, $slug);
        return back()->with('success', 'Service is successfully updated!');
    }

    public function destroy($id)
    {
        $this->_service->destroyService($id);
        return back()->with('success', 'Service is successfully Deleted!');
    }
}
