<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySetting;
use App\Contact;

class ContactController extends Controller
{
	private $_setting, $_contact;

    public function __construct()
	{
		$this->_setting = new CompanySetting();
		$this->_contact = new Contact();
	}

    public function index()
    {
    	$setting = $this->_setting::first();
    	return view('pre-login.pages.contact', compact('setting'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [

    		'fullname' => 'required',
    		'email' => 'required|email',
    		'message' => 'required'

    	]);

    	$fullname = $request->fullname;
    	$email = $request->email;
    	$message = $request->message;

        if($this->_contact->checkIfContactExists($fullname, $email, $message))
        {
            return response()->json(['status' => 'error', 'message' => 'Warning! message duplication!']);
        }

    	$this->_contact->storeContactMessage($fullname, $email, $message);
    	return response()->json(["status" => "success", "message" => "Your message is successfully sent!"]);
    }
}
