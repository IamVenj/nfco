<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomeCarousel;
use App\Services;
use App\Event;
use App\Project;
use App\CompanySetting;
use App\News;
use App\Sponsor;
use App\About;

class HomeController extends Controller
{
    private $_carousel, $_services, $_event, $_news, $_sponsor, $_setting, $_about;

    public function __construct()
    {
        $this->_carousel = new HomeCarousel();
        $this->_services = new Services();
        $this->_event = new Event();
        $this->_project = new Project();
        $this->_setting = new CompanySetting();
        $this->_news = new News();
        $this->_sponsor = new Sponsor();
        $this->_about = new About();
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $carousels = $this->_carousel::latest()->get();
        $services = $this->_services::take(4)->latest()->get();
        $latestEvent = $this->_event::take(1)->latest()->first();
        $projects = $this->_project::take(4)->latest()->get();
        $setting = $this->_setting::first();
        $twoLatestEvents = $this->_event::take(2)->latest()->get();
        $sponsors = $this->_sponsor::latest()->get();
        $newz = $this->_news::take(3)->latest()->get();
        $about = $this->_about::first();

        return view('pre-login.pages.home', compact('carousels', 'services', 'latestEvent', 'projects', 'setting', 'twoLatestEvents', 'sponsors', 'newz', 'about'));
    }
}
