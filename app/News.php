<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class News extends Model
{
    protected $fillable = ['title', 'description', 'public_id', 'version'];

    public function createNews($title, $description, $imageName)
    {
        if(!is_null($imageName))
        {
            $_image = new Image();
        	$uploadResult = $_image->addImage('News', $imageName);
        	
            $this::create([

            	'title' => $title,
            	'description' => $description,
            	'public_id' => $uploadResult['public_id'],
            	'version' => $uploadResult['version']

            ]);
        }
        else
        {
            $this::create([

                'title' => $title,
                'description' => $description

            ]);
        }
    }

    public function updateNewsText($id, $title, $description)
    {
    	$news = $this::find($id);

    	$news->title = $title;
    	$news->description = $description;

    	$news->save();
    }

    public function updateNewsImage($id, $imageName)
    {
        $_image = new Image();
    	$_image->updateImage($id, 'News', $imageName, 'App\News');
    }

    public function destroyNews($id)
    {
        $_image = new Image();
        $_image->deletePreviousImage($id, 'App\News');
        $this::find($id)->delete();
    }
}
