<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function _update_account_($email, $password, $id)
    {
        $user = $this::find($id);

        if(!is_null($password))
        {
            $user->email  = $email;
            $user->password = bcrypt($password);
        }
        else
        {
            $user->email = $email;
        }
    }

    public function isConnected()
    {
        $connected = @fsockopen("www.google.com", 80);

        if($connected)
            return true;
        else
            return false;
    }
}
