<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sponsor;

class Sponsor extends Model
{
    protected $fillable = ['link', 'public_id', 'version'];

    public function createSponsor($link, $imageName)
    {
        $_image = new Image();
    	$uploadResult = $_image->addImage('Sponsors', $imageName);

    	$this::create([

    		'link' => $link,
    		'public_id' => $uploadResult['public_id'],
    		'version' => $uploadResult['version']

    	]);
    }

    public function updateSponsorURL($id, $link)
    {
    	$sponsor = $this::find($id);
    	$sponsor->link = $link;
    	$sponsor->save();
    }

    public function updateSponsorImage($id, $imageName)
    {
        $_image = new Image();
    	$_image->updateImage($id, 'Sponsors', $imageName, 'App\Sponsor');
    }

    public function destroySponsor($id)
    {
        $_image = new Image();
    	$_image->deletePreviousImage($id, 'App\Sponsor');
    	$this::find($id)->delete();
    }
}
