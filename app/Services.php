<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $fillable = ['title', 'description'];

    public function createService($title, $description)
    {
    	$this::create([

    		'title' => $title,
    		'description' => $description

    	]);
    }

    public function updateService($id, $title, $description)
    {
    	$service = $this::find($id);

    	$service->title = $title;
    	$service->description = $description;

    	$service->save();
    }

    public function destroyService($id)
    {
    	$this::find($id)->delete();
    }
}
