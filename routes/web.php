<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'AboutController@index')->name('client.about');
Route::get('/services', 'ServicesController@index')->name('client.service.showall');

Route::get('/projects', 'ProjectController@index')->name('client.project.showall');
Route::get('/{project_name}/project/{id}', 'ProjectController@show')->name('client.project.show');

Route::get('/events', 'EventsController@index')->name('client.event.showall');
Route::get('/{event_name}/event/{id}', 'EventsController@show')->name('client.event.show');

Route::get('/news', 'NewsController@index')->name('client.news.showall');
Route::get('/{news_name}/news/{id}', 'NewsController@show')->name('client.news.show');

Route::get('/contact', 'ContactController@index')->name('client.contact');
Route::post('/contact', 'ContactController@store')->name('contact.store');

Route::get('/donate', 'DonationController@index')->name('donate.index');
Route::post('/donate', 'DonationController@store')->name('donate.store');

Auth::routes();

Route::get('/admin/dashboard', 'DashboardController@index');

Route::get('/admin/account', 'AdminAccountController@index')->name('account.index');
Route::patch('/admin/account', 'AdminAccountController@store')->name('account.store');

Route::get('/company-settings', 'CompanySettingsController@index');
Route::post('/company-settings', 'CompanySettingsController@store');

Route::get('/admin/sponsor', 'AdminPartnerController@index')->name('sponsor.index');
Route::post('/admin/sponsor', 'AdminPartnerController@store')->name('sponsor.store');
Route::patch('/admin/sponsor/{id}', 'AdminPartnerController@update')->name('sponsor.updateText');
Route::patch('/admin/sponsor/{id}/image', 'AdminPartnerController@updateImage')->name('sponsor.image');
Route::delete('/admin/sponsor/{id}', 'AdminPartnerController@destroy')->name('sponsor.destroy');

Route::get('/admin/create/news', 'AdminNewsController@create')->name('news.create');
Route::get('/admin/index/news', 'AdminNewsController@index')->name('news.index');
Route::post('/admin/create/news', 'AdminNewsController@store')->name('news.store');
Route::patch('/admin/news/{id}/image', 'AdminNewsController@updateImage')->name('news.image');
Route::patch('/admin/news/{id}', 'AdminNewsController@update')->name('news.updateText');
Route::delete('/admin/news/{id}', 'AdminNewsController@destroy')->name('news.destroy');

Route::get('/admin/create/project', 'AdminProjectController@create')->name('project.create');
Route::get('/admin/index/project', 'AdminProjectController@index')->name('project.index');
Route::post('/admin/create/project', 'AdminProjectController@store')->name('project.store');
Route::patch('/admin/project/{id}/image', 'AdminProjectController@updateImage')->name('project.image');
Route::patch('/admin/project/{id}', 'AdminProjectController@update')->name('project.updateText');
Route::delete('/admin/project/{id}', 'AdminProjectController@destroy')->name('project.destroy');

Route::get('/admin/service', 'AdminServiceController@index')->name('service.index');
Route::patch('/admin/service/{id}', 'AdminServiceController@update')->name('service.updateText');
Route::delete('/admin/service/{id}', 'AdminServiceController@destroy')->name('service.destroy');
Route::get('/admin/create/service', 'AdminServiceController@create')->name('service.create');
Route::post('/admin/create/service', 'AdminServiceController@store')->name('service.store');

Route::get('/admin/event', 'AdminEventController@index')->name('event.index');
Route::patch('/admin/event/{id}', 'AdminEventController@update')->name('event.updateText');
Route::patch('/admin/event/{id}/image', 'AdminEventController@updateImage')->name('event.image');
Route::delete('/admin/event/{id}', 'AdminEventController@destroy')->name('event.destroy');
Route::get('/admin/create/event', 'AdminEventController@create')->name('event.create');
Route::post('/admin/create/event', 'AdminEventController@store')->name('event.store');

Route::get('/admin/carousel', 'AdminCarouselController@index')->name('carousel.index');
Route::patch('/admin/carousel/{id}', 'AdminCarouselController@update')->name('carousel.updateText');
Route::patch('/admin/carousel/{id}/image', 'AdminCarouselController@updateImage')->name('carousel.image');
Route::delete('/admin/carousel/{id}', 'AdminCarouselController@destroy')->name('carousel.destroy');
Route::get('/admin/create/carousel', 'AdminCarouselController@create')->name('carousel.create');
Route::post('/admin/create/carousel', 'AdminCarouselController@store')->name('carousel.store');

Route::get('/admin/contact', 'AdminContactController@index')->name('admin.contact.index');

/*
/----------------------
/ -- Custom Pages --
/-----------------
*/

Route::get('/admin/about', 'AdminAboutController@index')->name('admin.about.index');
Route::post('/admin/about', 'AdminAboutController@update')->name('admin.about.update');
Route::get('/admin/all', 'CustomHomeController@index')->name('admin.all.index');
Route::post('/admin/all', 'CustomHomeController@store')->name('admin.all.store');

