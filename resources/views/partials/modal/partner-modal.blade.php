<div class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog" id=<?= 'edit-partner-image'.$partner->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3" style="background: #ffaf00; color: #fff;">

                <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Sponsor Image</h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="{{route('sponsor.image', ['id' => $partner->id])}}" method="post" enctype="multipart/form-data">
                
                @csrf

                @METHOD('patch')                

                <div class="modal-body">

                    <div class="form-group">

                        <label for="image" style="font-size: 15px;">Image <small style="color: red;">*</small></label>

                        <input type="file" name="image" class="form-control" id="image" placeholder="Image" style="font-size: 15px; padding-bottom: 40px;" required="" onchange="readURL(this)">

                    </div>

                    @if(!is_null($partner->public_id))

                    <img src="<?= Cloudder::show($partner->public_id, ['version'=> $partner->version, 'width'=>250, 'height'=>250, 'crop'=>'fill']);?>" alt="{{$partner->link}}" id="img" style="width: auto; height: 200px; object-fit: cover;">

                    @endif

                </div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-warning" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Save Changes</button>

                </div>

            </form>


        </div>

    </div>

</div>

<div class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog" id=<?= 'edit-partner'.$partner->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3" style="background: #ffaf00; color: #fff;">

                <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Sponsor</h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="{{route('sponsor.updateText', ['id' => $partner->id])}}" method="post" enctype="multipart/form-data">
                
                @csrf

                @METHOD('patch')                

                <div class="modal-body">

                    <div class="form-group">

                        <label for="link" style="font-size: 15px;">website URL</label>

                        <input type="text" class="form-control" name="link" id="link" placeholder="webURL" style="font-size: 15px;" value="{{$partner->link}}">

                    </div>

                </div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-warning" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Save Changes</button>

                </div>

            </form>


        </div>

    </div>

</div>

<div class="modal fade" role="dialog" style="border: none;" id=<?= 'delete-partner'.$partner->id ?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <form action="{{route('sponsor.destroy', ['id' => $partner->id])}}" method="post">
                
                @csrf

                @method('DELETE')

                <div class="modal-header header-color-modal bg-color-4" style="background: rgb(230, 82, 81); color: #fff;">

                    <h4 class="modal-title" style="font-size: 17px;"><i class="fa fa-trash-o"></i> Delete Partner</h4>

                    <div class="modal-close-area modal-close-df">

                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                    </div>

                </div>


                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-danger" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to Delete</button>

                </div>

            </form>

        </div>

    </div>

</div>
