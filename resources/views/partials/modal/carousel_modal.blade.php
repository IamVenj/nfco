<div class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog" id=<?= 'edit-carousel'.$carousel->id ?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Carousel</h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="{{route('carousel.updateText', ['id' => $carousel->id])}}" method="post">
                
                @csrf

                @METHOD('PATCH')

                <div class="modal-body">

                    <div class="form-group">

                        <label for="name" style="font-size: 15px;">Title <small style="color: red;">*</small></label>

                        <input type="text" class="form-control" value="{{$carousel->title}}" name="title" id="name" placeholder="Title" style="font-size: 15px; font-weight: bold;">

                    </div>

                    <div class="form-group">

                        <label for="name" style="font-size: 15px;">MiniTitle <small style="color: red;">*</small></label>

                        <input type="text" class="form-control" value="{{$carousel->mini_title}}" name="mini_title" id="name" placeholder="Mini Title" style="font-size: 15px; font-weight: bold;" required>

                    </div>

                    <div class="form-group">

                        <label for="slug" style="font-size: 15px;">Short Slug/Description <small style="color: red;">*</small></label>

                        <textarea class="form-control slug" rows="5" name="slug" id="slug" placeholder="Description" style="font-size: 15px; font-weight: bold;" required>{{$carousel->description}}</textarea>

                    </div>

                </div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-primary" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Save Changes</button>

                </div>

            </form>


        </div>

    </div>

</div>

<div class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog" id=<?= 'update-image-carousel'.$carousel->id; ?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Carousel Image</h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="{{route('carousel.image', ['id' => $carousel->id])}}" method="post" enctype="multipart/form-data">
                
                @csrf

                @METHOD('patch')                

                <div class="modal-body">

                    <div class="form-group">

                        <label for="image" style="font-size: 15px;">Image <small style="color: red;">*</small></label>

                        <input type="file" name="image" class="form-control" id="image" placeholder="Image" style="font-size: 15px; font-weight: bold; padding-bottom: 40px;" onchange="readURL(this)">

                    </div>

                    <img src="<?= Cloudder::show($carousel->public_id, ['version'=> $carousel->version, 'width'=>400, 'height'=>400, 'crop'=>'fill']);?>" alt="{{$carousel->title}}" class="img-card" id="img" style="width: auto !important; height: 200px; object-fit: cover;">

                </div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-primary" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Save Changes</button>

                </div>

            </form>


        </div>

    </div>

</div>

<div class="modal fade" role="dialog" style="border: none;" id=<?= 'delete-carousel'.$carousel->id; ?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <form action="{{route('carousel.destroy', ['id' => $carousel->id])}}" method="post">
                
                @csrf

                @method('DELETE')

                <div class="modal-header header-color-modal bg-color-4" style="background: rgb(230, 82, 81); color: #fff;">

                    <h4 class="modal-title" style="font-size: 17px;"><i class="fa fa-trash-o"></i> Delete Carousel</h4>

                    <div class="modal-close-area modal-close-df">

                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                    </div>

                </div>


                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-danger" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to Delete</button>

                </div>

            </form>

            

        </div>

    </div>

</div>
