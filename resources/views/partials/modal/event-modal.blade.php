<div class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog" id=<?= 'edit-event'.$event->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Event </h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="{{route('event.updateText', ['id' => $event->id])}}" method="post">
                
                @csrf

                @METHOD('PATCH')

                <div class="modal-body">

                    <div class="form-group">

                        <label for="name" style="font-size: 15px;">Event Name <small style="color: red;">*</small></label>

                        <input type="text" class="form-control" name="name" id="name" placeholder="event name" style="font-size: 15px;" value="{{$event->event_name}}" required="">

                    </div>
                        
                    <div class="form-group">
                        <label for="dtp_input2" class="control-label">Date: from</label>
                        <div class="input-group date form_date" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtp_input2" data-link-format="dd/mm/yyyy">
                            <input class="form-control" size="16" type="text" value="" name="date_from" readonly style="font-size: 16px;" value="{{$event->date_from->format('d/m/y')}}">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->
                        </div>
                        <input type="hidden" id="dtp_input2" value="" /><br/>
                    </div>
                        
                    <div class="form-group">
                        <label for="dtp_input2" class="control-label">Date: to</label>
                        <div class="input-group date form_date" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                            <input class="form-control" size="16" type="text" name="date_to" value="" readonly style="font-size: 16px;" value="{{$event->date_to->format('d/m/y')}}">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->
                        </div>
                        <input type="hidden" id="dtp_input2" value="" /><br/>
                    </div>
                        
                    <div class="form-group">
                        <label for="dtp_input3">Select Time</label>
                        <div class="input-group date form_time" data-date="" data-date-format="h:i" data-link-field="dtp_input3" data-link-format="h:i">
                            <input class="form-control" size="16" type="text" value="" name="time" readonly style="font-size: 16px;">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span> -->
                        </div>
                        <input type="hidden" id="dtp_input3" value="" /><br/>
                    </div>

                    <div class="form-group">

                        <label for="slug" style="font-size: 15px;">slug <small style="color: red;">*</small></label>

                        <textarea class="form-control"  rows="8" name="slug" id="slug" placeholder="Description/slug" style="font-size: 15px;" required><?= $event->event_slug;?></textarea>

                    </div>

                    <div class="form-group">

                        <label for="location" style="font-size: 15px;">Location <small style="color: red;">*</small></label>

                        <input type="text" class="form-control" name="location" id="location" placeholder="Location" style="font-size: 15px;" required="" value="{{$event->location}}">

                    </div>

                </div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-primary" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Save Changes</button>

                </div>

            </form>


        </div>

    </div>

</div>

<div class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog" id=<?= 'edit-event-image'.$event->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Event Image</h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="{{route('event.image', ['id' => $event->id])}}" method="post" enctype="multipart/form-data">
                
                @csrf

                @METHOD('patch')                

                <div class="modal-body">

                   <div class="form-group">

                        <label for="image" style="font-size: 15px;">Image</label>

                        <input type="file" name="image" class="form-control" id="image" placeholder="Image" style="font-size: 15px; padding-bottom: 40px;" onchange="readURL(this)">

                    </div>

                    @if(!is_null($event->public_id))

                    <img src="<?= Cloudder::show($event->public_id, ['version'=> $event->version, 'width'=>350, 'height'=>350, 'crop'=>'fill']);?>" class="img-card" id="img" style="width: auto; height: 200px; object-fit: cover;">

                    @endif

                </div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-primary" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Save Changes</button>

                </div>

            </form>


        </div>

    </div>

</div>

<div class="modal fade" role="dialog" style="border: none;" id=<?= 'delete-event'.$event->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <form action="{{route('event.destroy', ['id' => $event->id])}}" method="post">
                
                @csrf

                @method('DELETE')

                <div class="modal-header header-color-modal bg-color-4" style="background: rgb(230, 82, 81); color: #fff;">

                    <h4 class="modal-title" style="font-size: 17px;"><i class="fa fa-trash-o"></i> Delete Event</h4>

                    <div class="modal-close-area modal-close-df">

                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                    </div>

                </div>


                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-danger" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to Delete</button>

                </div>

            </form>

            

        </div>

    </div>

</div>
