@extends('post-login.index.header')

@section('content')

@include('partials.error2')

@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

			<div class="col-lg-12 grid-margin stretch-card">

        <div class="card">

          <div class="card-body">

                <h4 class="card-title" style="font-size:17px; text-transform:uppercase;"><b>Donations</b></h4>

                <p class="card-description" style="font-size:15px; color:rgba(0,0,0,0.7);"> </p>

            <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

            <div class="table-responsive">

              <table class="table table-hover" id="dataTables-example">

                <thead>

                  <tr>

                    <th style="font-size: 18px;">donation amount</th>
                    <th style="font-size: 18px;">name</th>
                    <th style="font-size: 18px;">email</th>
                    <th style="font-size: 18px;">phone number</th>
                    <th style="font-size: 18px;">address1</th>
                    <th style="font-size: 18px;">address2</th>

                  </tr>

                </thead>

                <tbody>

                  @foreach($donations as $donation)

                  <tr class="record">
                    <td style="font-size: 15px;">{{$donation->amount}}</td>
                    <td style="font-size: 15px;">{{$donation->firstname.' '.$donation->lastname}}</td>
                    <td style="font-size: 15px;">{{$donation->email}}</td>
                    <td style="font-size: 15px;">{{$donation->phone_number}}</td>
                    <td style="font-size: 15px;">{{$donation->address1}}</td>
                    <td style="font-size: 15px;">{{$donation->address2}}</td>
                  </tr>

                  @endforeach

                </tbody>

              </table>

            </div>

          </div>

        </div>

      </div>

		</div>

	</div>

	@include('post-login.index.footer')

</div>

<script src="js/jquery.min.js"></script>

<script>

$(document).ready(function() {

  $('#dataTables-example').DataTable({

    responsive: true

  });

});

</script> 


@endsection