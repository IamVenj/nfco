@extends('post-login.index.header')

@section('content')

<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              
              <div class="float-left">
                <i class="mdi mdi-account-location text-info icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right" style="font-size: 14px;">Contacts</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{App\Contact::count()}}</h3>
                </div>
              </div>
            </div>
             <p class="text-muted mt-3 mb-0" style="font-size: 14px;">
              <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Number of contacts
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-format-list-bulleted text-warning icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right" style="font-size: 14px;">Services</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{App\Services::count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0" style="font-size: 14px;">
              <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Number of created services
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-eventbrite text-success icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right" style="font-size: 14px;">Events</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{App\Event::count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0" style="font-size: 14px;">
              <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Number of created Events
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-projector-screen text-danger icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right" style="font-size: 14px;">Projects</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{App\Project::count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0" style="font-size: 14px;">
              <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Number of Projects
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-newspaper text-primary icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right" style="font-size: 14px;">News</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{App\News::count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0" style="font-size: 14px;">
              <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Number of News Created
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-poll-box text-success icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right" style="font-size: 14px;">Carousel</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{App\HomeCarousel::count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0" style="font-size: 14px;">
              <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Number of created Carousel
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-account-multiple-plus-outline text-info icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right" style="font-size: 14px;">Sponsors</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{App\Sponsor::count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0" style="font-size: 14px;">
              <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Number of created Sponsors
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-cash text-success icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right" style="font-size: 14px;">Donations</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{App\Donation::count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0" style="font-size: 14px;">
              <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Number of given donations
            </p>
          </div>
        </div>
      </div>
    </div>
    
</div>
 @include('post-login.index.footer')
@endsection