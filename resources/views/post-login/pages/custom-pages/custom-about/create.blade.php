@extends('post-login.index.header')

@section('content')

@include('partials.error2')

@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

	    	<div class="col-md-12 d-flex align-items-stretch grid-margin">

	      		<div class="row flex-grow">

	        		<div class="col-12">

	          			<div class="card">

	            			<div class="card-body">

	            				<h4 class="card-title">Custom-About</h4>

	            				<div class="dropdown-divider w-25"></div>

              					<p class="card-description mt-2 mb-2">

                					Customize your about-page

              					</p>

              					<div class="dropdown-divider w-25 "></div>

              					<form class="forms-sample mt-4" action="{{route('admin.about.update')}}" method="post" enctype="multipart/form-data">

              						@csrf

              						<div class="row">
						               
						                <div class="col-md-12">
						                	
							                <div class="form-group">
							                
							                  	<label for="slug" style="font-size: 15px;">The companies mission <small style="color: red;">*</small></label>
							                
							                  	<textarea class="form-control"  rows="7" name="mission" id="slug" placeholder="The companies mission" style="font-size: 15px; " required>{{$about->mission}}</textarea>
							                
							                </div>

							             </div>

							             <div class="col-md-12">
							                
							                <div class="form-group">
							                
							                  	<label for="slug2" style="font-size: 15px;">The companies vision </label>
							                
							                  	<textarea class="form-control"  rows="7" name="vision" id="slug2" placeholder="The companies vision" style="font-size: 15px;" >{{$about->vision}}</textarea>
							                
							                </div>
						                

						                </div>

						                <div class="col-md-12">
						                	
							                <div class="form-group">
							                
							                  	<label for="slug3" style="font-size: 15px;">The Companies Objective </label>
							                
							                  	<textarea class="form-control"  rows="7" name="objective" id="slug3" placeholder="detailed description about your company 3" style="font-size: 15px;">{{$about->objective}}</textarea>
							                
							                </div>

							            </div>

							            <div class="col-md-12">

							                <div class="form-group">
							                
							                  	<label for="slug4" style="font-size: 15px;">Who are we??</label>
							                
							                  	<textarea class="form-control"  rows="7" name="who_are_we" id="slug4" placeholder="Who are we??" style="font-size: 15px;" >{{$about->who_are_we}}</textarea>
							                
							                </div>

						                </div>

						                <div class="col-md-12">

							                <div class="form-group">
							                
							                  	<label for="quote" style="font-size: 15px;">Company Motto  </label>
							                
							                  	<input class="form-control" type="text" name="motto" id="quote" placeholder="your company Motto" value="{{$about->motto}}" style="font-size: 15px;" >
							                
							                </div>

						                </div>

						                

              						</div>
					                
					               
					                <button type="submit" class="btn btn-primary mr-2 mt-4"><i class="mdi mdi-cloud-download"></i>Update</button>

              					</form>

	            			</div>

	          			</div>

	        		</div>

	    		</div>

			</div>

		</div>

	</div>

	@include('post-login.index.footer')

</div>

<script src="js/jquery.min.js"></script>

<script type="text/javascript">

  $(document).ready(function() {

    $("#slug").wysihtml5();

    $("#slug2").wysihtml5();

    $("#slug3").wysihtml5();
    
    $("#slug4").wysihtml5();

  });

</script>

@endsection