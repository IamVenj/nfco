@extends('post-login.index.header')

@section('content')

@include('partials.error2')

@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

	    	<div class="col-md-12 d-flex align-items-stretch grid-margin">

	      		<div class="row flex-grow">

	        		<div class="col-12">

	          			<div class="card">

	            			<div class="card-body">

	            				<h4 class="card-title">Custom-All</h4>

	            				<div class="dropdown-divider w-25"></div>

              					<p class="card-description mt-2 mb-2">

                					Customize All

              					</p>

              					<div class="dropdown-divider w-25 "></div>

              					<form class="forms-sample mt-4" action="{{route('admin.all.store')}}" method="post" enctype="multipart/form-data">
					                @csrf

					                <div class="row">

					                	<div class="col-md-6">
					                		
							                <div class="form-group">
							                
							                  	<label for="image" style="font-size: 15px;">Main Photo <small style="color: red;">*</small></label>
							                
							                  	<input type="file" name="mainImage" class="form-control" id="image" placeholder="Main Image" onchange="readURL(this)" style="font-size: 15px; padding-bottom: 40px;">
							                
							                </div>
							                
							                <label style="font-size: 15px; font-weight: bold">Current Main Photo <i class="mdi mdi-arrow-down"></i></label>

							                <div class="form-group">

							                  	<img src="{{URL::asset('storage/uploads/custom-pages/all/main-img.jpg')}}" class="img-card mt-4" id="img" style="width: auto; height: 200px;">

							                </div>

					                	</div>

					                	<div class="col-md-6">
					                		
							                <div class="form-group">
							                
							                  	<label for="image" style="font-size: 15px;">Cover Photo <small style="color: red;">*</small></label>
							                
							                  	<input type="file" name="coverImage" class="form-control" id="image" placeholder="Main Courses Image" onchange="readURL2(this)" style="font-size: 15px; padding-bottom: 40px;">
							                
							                </div>
							                
							                <label style="font-size: 15px; font-weight: bold">Current Cover Image <i class="mdi mdi-arrow-down"></i></label>

							                <div class="form-group">

							                  	<img src="{{URL::asset('storage/uploads/custom-pages/all/cover-img.jpg')}}" class="img-card mt-4" id="img2" style="width: auto; height: 200px;">

							                </div>

					                	</div>
					                	

					                </div>

					                
					                <button type="submit" class="btn btn-primary mr-2 mt-4"><i class="mdi mdi-cloud-download"></i>Update</button>

              					</form>

	            			</div>

	          			</div>

	        		</div>

	    		</div>

			</div>

		</div>

	</div>

	@include('post-login.index.footer')

</div>

@endsection