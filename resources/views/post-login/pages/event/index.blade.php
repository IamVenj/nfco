@extends('post-login.index.header')

@section('content')

@include('partials.error2')

@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

	    	<div class="col-md-12 d-flex align-items-stretch grid-margin">

	      		<div class="row flex-grow">

	        		<div class="col-12">

	          			<div class="card">

	            			<div class="card-body">

	            				<div class="row">

	            					<div class="col-md-6">

	              						<h4 class="card-title">Event</h4>

	            					</div>

	            					<div class="col-md-6">

			              				<a class="btn btn-success btn-block" href="{{route('event.create')}}" style="color: #fff;">		

							                <i class="mdi mdi-plus"></i> Add Event

							            </a>

	            					</div>

	            				</div>

	            				<div class="dropdown-divider w-25"></div>

	            				<div class="row">

	            					@if(!is_null($events))

	            					@foreach($events as $event)

	            					@include('partials.modal.event-modal')

		            				<div class="col-md-4 mt-3">

		              					<div class="card2">

		              						@if(!is_null($event->public_id))
		                                    
		                                    <img class="card-img-top" style="height: 250px; object-fit: cover;" src="<?= Cloudder::show($event->public_id, ['version'=> $event->version, 'width'=>350, 'height'=>350, 'crop'=>'fill']);?>" alt="$event->image">		       

		                                    @else

		                                    <div style="padding-top: 51%; border-top-left-radius: 10px; border-top-right-radius: 10px;  background: linear-gradient(120deg, #00e4d0, #429FFD);" alt="Card image cap"></div>

		                                    @endif                          

		                                    <div class="card-body">

		                                    	
		                                        <h4 class="card-title mb-3" style="font-weight: bold;">{{$event->event_name}}</h4>

		                                        <p style="font-size: 14px;"><?= \Str::limit($event->event_slug, 50);?></p>

		                                        <div class="ticket-actions mb-3">

							                        <div class="btn-group dropdown">

							                          	<button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

							                            	Manage

							                          	</button>

							                          	<div class="dropdown-menu">

							                            	<a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#edit-event'.$event->id;?>">

							                              		<i class="mdi mdi-tooltip-edit mr-2" style="color: rgba(0,0,0,0.5);"></i>Edit</a>

							                            	<div class="dropdown-divider"></div>

							                            	<a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#edit-event-image'.$event->id;?>">

							                              		<i class="mdi mdi-tooltip-edit mr-2" style="color: rgba(0,0,0,0.5);"></i>Edit Event Image</a>

							                            	<div class="dropdown-divider"></div>

							                            	<a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#delete-event'.$event->id;?>">

							                              		<i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>Delete</a>

							                          	</div>

							                        </div>

							                    </div>

		                                        <p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 14px;"><i class="mdi mdi-clock"></i> {{$event->created_at->diffForHumans()}}</p> 

		                                        <div class="dropdown-divider"></div>

		                                        <div class="row">

		                                        	<div class="col-md-12">
		                                        		<p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 14px;"><i class="mdi mdi-map-marker"></i> {{$event->location}}</p>	                                        		
		                                        	</div>

		                                        </div>

		                                        <div class="dropdown-divider"></div>

		                                         <div class="row">
		                                        	
		                                        	<div class="col-md-12">
		                                        		<p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 14px;"><i class="mdi mdi-calendar"></i> Date | {{$event->date_from->format('M d, y')}} - {{$event->date_to->format('M d, y')}}</p>
		                                        	</div>

		                                        	<div class="col-md-12">		                                        		
		                                        		<p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 14px;"><i class="mdi mdi-clock"></i> Time | {{$event->time}}</p> 
		                                        	</div>

		                                        </div>

		                                    </div>

		                                </div>

		            				</div>  

		            				@endforeach

		            				@endif        				

	            				</div>

	            			</div>

	          			</div>

	        		</div>

	    		</div>

			</div>

		</div>

	</div>

	@include('post-login.index.footer')

</div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>
 
<script src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

  $(document).ready(function() {

    $("#slug").wysihtml5();

  });

  $('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });

  $('.form_time').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0,
		showMeridian: 1
    });

</script>

@endsection