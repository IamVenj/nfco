
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
   
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <div class="user-wrapper">
          <div class="text-wrapper text-center">
            <p class="profile-name  text-center" style="font-size: 20px;">{{auth()->user()->name}}</p>
            <div>
              <small class="designation text-muted">{{auth()->user()->email}}</small>
              <span class="status-indicator online"></span>
            </div>
          </div>
        </div>
      </div>
    </li>
    
    <li class="nav-item">
      <a class="nav-link" href="/admin/dashboard">
        <i class="menu-icon mdi mdi-television"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
        <i class="menu-icon mdi mdi-content-copy"></i>
        <span class="menu-title">Customize Pages</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-basic">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="{{route('admin.all.index')}}">All Page Customization</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('admin.about.index')}}">Customize About</a>
          </li>          
        </ul>
      </div>
    </li>
   <li class="nav-item">
      <a class="nav-link" href="{{route('carousel.index')}}">
        <i class="menu-icon mdi mdi-poll-box"></i>
        <span class="menu-title">Carousel</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('service.index')}}">
        <i class="menu-icon mdi mdi-format-list-bulleted"></i>
        <span class="menu-title">Service</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('news.index')}}">
        <i class="menu-icon mdi mdi-newspaper"></i>
        <span class="menu-title">News</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('project.index')}}">
        <i class="menu-icon mdi mdi-projector-screen"></i>
        <span class="menu-title">Projects</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('sponsor.index')}}">
        <i class="menu-icon mdi mdi-account-multiple-plus-outline"></i>
        <span class="menu-title">Sponsors</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('event.index')}}">
        <i class="menu-icon mdi mdi-eventbrite"></i>
        <span class="menu-title">Events</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('admin.contact.index')}}">
        <i class="menu-icon mdi mdi-account"></i>
        <span class="menu-title">Contact</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('donate.index')}}">
        <i class="menu-icon mdi mdi-cash-multiple"></i>
        <span class="menu-title">Donations</span>
      </a>
    </li>
    
  </ul>
</nav>