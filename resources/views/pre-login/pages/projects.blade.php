@extends('pre-login.index.index')

@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/main-img.jpg')}});">
	<div class="auto-container">
    	<div class="sec-title">
            <h1>Our <span class="normal-font">Projects</span></h1>
            <div class="bread-crumb"><a href="/">Home</a> / <a href="/projects" class="current">Projects</a></div>
        </div>
    </div>
</section>


<!--Projects Section-->
<section class="projects-section">
	<div class="auto-container">
        
    	<div class="row clearfix">

             @if($projects->count() == 0)
            <div class="column blog-news-column col-xs-12">
                <p class="alert alert-danger text-center"><i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-right: 5px;"></i>No Projects are added yet<i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
            </div>
            @else
            @foreach($projects as $project)
            <div class="column default-featured-column col-md-3 col-sm-6 col-xs-12">
                <article class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <figure class="image-box">
                        <a href="{{route('client.project.show', ['id' => $project->id, 'project_name' =>$project->title])}}"><img src="<?= Cloudder::show($project->public_id, ['version'=> $project->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>" alt="{{$project->title}}"></a>
                    </figure>
                    <div class="content-box">
                        <h3><a href="{{route('client.project.show', ['id' => $project->id, 'project_name' =>$project->title])}}">{{$project->title}}</a></h3>
                        <div class="text"><?= \Str::limit($project->description, 35);?></div>
                        <a href="{{route('client.project.show', ['id' => $project->id, 'project_name' =>$project->title])}}" class="theme-btn btn-style-three">Learn More</a>
                    </div>
                </article>
            </div>
            @endforeach    
            @endif    
            
            
        </div>
        
        <!-- Styled Pagination -->
        <div class="styled-pagination padd-top-20 margin-bott-40">
            <ul>
                <li>{{$projects->links()}}</li>
            </ul>
        </div>
        
    </div>
</section>

@endsection