@extends('pre-login.index.index')

@section('content')

<!--Main Slider-->
<section class="main-slider revolution-slider">

    @if($carousels->count() > 0)
	
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>

                @foreach($carousels as $carousel)
            	
                <li data-transition="fade" data-slotamount="{{$carousel->id}}" data-masterspeed="1000" data-thumb="<?= Cloudder::show($carousel->public_id, ['version'=> $carousel->version, 'width'=>250, 'height'=>250, 'crop'=>'fill']);?>"  data-saveperformance="off"  data-title="{{$carousel->title}}">
                
	                <img src="<?= Cloudder::show($carousel->public_id, ['version'=> $carousel->version, 'width'=>1920, 'height'=>1920, 'crop'=>'fill']);?>"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
	                
	                <div class="tp-caption sfr sfb tp-resizeme"
	                data-x="left" data-hoffset="90"
	                data-y="center" data-voffset="-50"
	                data-speed="1500"
	                data-start="1000"
	                data-easing="easeOutExpo"
	                data-splitin="none"
	                data-splitout="none"
	                data-elementdelay="0.01"
	                data-endelementdelay="0.3"
	                data-endspeed="1200"
	                data-endeasing="Power4.easeIn"><h1>{{$carousel->title}}</h1></div>
	                
	                <div class="tp-caption sfl sfb tp-resizeme"
	                data-x="left" data-hoffset="90"
	                data-y="center" data-voffset="40"
	                data-speed="1500"
	                data-start="1500"
	                data-easing="easeOutExpo"
	                data-splitin="none"
	                data-splitout="none"
	                data-elementdelay="0.01"
	                data-endelementdelay="0.3"
	                data-endspeed="1200"
	                data-endeasing="Power4.easeIn"><h3 class="bg-color">{{$carousel->mini_title}}</h3></div>
	                
	                <div class="tp-caption"
	                data-x="left" data-hoffset="90"
	                data-y="center" data-voffset="100"
	                data-speed="1500"
	                data-start="2000"
	                data-easing="easeOutExpo"
	                data-splitin="none"
	                data-splitout="none"
	                data-elementdelay="0.01"
	                data-endelementdelay="0.3"
	                data-endspeed="1200"
	                data-endeasing="Power4.easeIn"><div class="text" style="min-width: 10px !important;"><?= $carousel->description;?></div></div>
   
                </li>

                @endforeach                
                
            </ul>
            
        </div>
    </div>
    @else
    <p class="alert alert-danger text-center"><i class="mdi mdi-emoticon-sad text-danger" style="font-size: 26px; margin-right: 5px;"></i>Dear Admin, Please add a carousel!<i class="mdi mdi-emoticon-sad text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
    @endif
</section>


<!--Main Features-->
<section class="main-features">
	<div class="auto-container">
    	<div class="sec-title clearfix">
            <div class="pull-left">
                <h2>Our <span class="normal-font theme_color">Services</span></h2>
            </div>
            @if(App\Services::all()->count() > 4)
            <div class="pull-right padd-top-30">
                <a href="{{route('client.service.showall')}}" class="theme-btn btn-style-three">See All Services</a>
            </div>
            @endif
        </div>
        
        <div class="row clearfix">

            @if($services->count() == 0)

            <div class="column blog-news-column col-xs-12">
                <p class="alert alert-danger text-center"><i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-right: 5px;"></i>No Service are added yet<i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
            </div>

            @else
        	
            @foreach($services as $service)

            <!--Feature Column-->
            <div class="features-column col-lg-3 col-md-6 col-xs-12">
            	<article class="inner-box">
                	<div class="icon-box">
                        <h3 class="title">{{$service->title}}</h3>
                    </div>
                </article>
            </div>

            @endforeach

            @endif

        </div>
    </div>
</section>

@if(!is_null($latestEvent))

<section class="featured-fluid-section">
	
    
	<div class="outer clearfix">
        
        <div class="image-column" style="background-image:url(<?= Cloudder::show($latestEvent->public_id, ['version'=> $latestEvent->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>);"></div>
        
        <article class="column text-column dark-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms" style="background-image:url(images/resource/fluid-image-2.jpg);">
            
            <div class="content-box pull-left">	
                <h2>Join <span class="theme_color">our event</span> &amp; help us by donation</h2>
            	<div class="title-text"> <a href="#"><strong>{{$latestEvent->event_name}}</strong></a> </div>
                <div class="text"><?= $latestEvent->event_slug;?></div>
                <a href="{{route('client.event.show', ['id'=>$latestEvent->id, 'event_name'=>$latestEvent->event_name])}}" class="theme-btn btn-style-two">View details</a>
            </div>
            
            <div class="clearfix"></div>
        </article>
        
    </div>
    
</section>
@endif


<section class="recent-projects">
	<div class="auto-container">
    	
        <div class="sec-title clearfix">
        	<div class="pull-left">
                <h2>RECENT <span class="normal-font theme_color">Project</span></h2>
            </div>
            @if(App\Project::all()->count() > 4)
            <div class="pull-right padd-top-30">
            	<a href="{{route('client.project.showall')}}" class="theme-btn btn-style-three">See All Projects</a>
            </div>
            @endif
        </div>
    	<div class="row clearfix">
            @if($projects->count() == 0)
            <div class="column blog-news-column col-xs-12">
                <p class="alert alert-danger text-center"><i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-right: 5px;"></i>No Projects are added yet<i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
            </div>
            @else
            @foreach($projects as $project)
            <div class="column default-featured-column col-md-3 col-sm-6 col-xs-12">
            	<article class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
            		<figure class="image-box">
                    	<a href="{{route('client.project.show', ['id' => $project->id, 'project_name' =>$project->title])}}"><img src="<?= Cloudder::show($project->public_id, ['version'=> $project->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>" alt="{{$project->title}}"></a>
                    </figure>
                    <div class="content-box">
                    	<h3><a href="{{route('client.project.show', ['id' => $project->id, 'project_name' =>$project->title])}}">{{$project->title}}</a></h3>
                        <div class="text"><?= \Str::limit($project->description);?></div>
                        <a href="{{route('client.project.show', ['id' => $project->id, 'project_name' =>$project->title])}}" class="theme-btn btn-style-three">Learn More</a>
                    </div>
                </article>
            </div>
            @endforeach    
            @endif        
        </div>
    </div>
</section>

<!--Two Column Fluid -->
<section class="two-column-fluid">
    
    <div class="outer clearfix">
        
        <article class="column left-column" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/cover-img.jpg')}});">
            
            <div class="content-box pull-right">    
                <h2><span class="normal-font theme_color">Who</span> are we?</h2>
                <div class="title-text">{{$setting->company_name}}</div>
                <div class="text"><?= $about->who_are_we;?></div>
                <br>
                
                <div class="clearfix">
                    <div class="icon-box">
                        <div class="icon"><span class="flaticon-shapes-1"></span></div>
                        <div class="lower-box">
                            <h4><span class="count-text" data-stop="7845" data-speed="1500">{{App\Event::count()}}</span></h4>
                            <span class="title">Events</span>
                        </div>
                    </div>
                    
                    <div class="icon-box">
                        <div class="icon"><span class="flaticon-tool-4"></span></div>
                        <div class="lower-box">
                            <h4><span class="count-text" data-stop="13360" data-speed="1500">{{App\Project::count()}}</span></h4>
                            <span class="title">Projects</span>
                        </div>
                    </div>
                    
                    <div class="icon-box">
                        <div class="icon"><span class="flaticon-favorite"></span></div>
                        <div class="lower-box">
                            <h4><span class="count-text" data-stop="78459" data-speed="1500">{{App\Donation::count()}}</span></h4>
                            <span class="title">Donations</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </article>
        
        <article class="column right-column" style="background-image:url(images/resource/fluid-image-4.jpg);">
            
            <div class="content-box pull-left"> 
                <div class="outer-box">
                    <div class="quote-icon"><span class="fa fa-quote-left"></span></div>
                    <h2>Our Motto</h2>
                    
                    <!--Text Content-->
                    <div class="text-content">
                        <div class="text"><p><?= $about->motto;?></p></div>
                        
                    </div>
                    
                </div>
            </div>
            
            <div class="clearfix"></div>
        </article>
        
    </div>
    
</section>

<!--Events Section-->
<section class="events-section latest-events">
	<div class="auto-container">
    	
        <div class="sec-title text-center">
            <h2>Latest <span class="normal-font theme_color">Event</span></h2>
            @if(App\Event::count() > 6)
            <div class="pull-right">
                <a href="{{route('client.event.showall')}}" class="theme-btn btn-style-three">See All Events</a>
            </div>
            @endif
        </div>
    	<div class="row clearfix">
            @if(is_null($latestEvent))
            <div class="column blog-news-column col-xs-12">
                <p class="alert alert-danger text-center"><i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-right: 5px;"></i>No Events are added yet<i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
            </div>
            @else
            @foreach($twoLatestEvents as $_event)
            
            <!--Featured Column-->
            <div class="column default-featured-column style-two col-lg-4 col-md-6 col-sm-6 col-xs-12">
            	<article class="inner-box">
            		<figure class="image-box">
                    	<a href="{{route('client.event.show', ['id' => $_event->id, 'event_name' => $_event->event_name])}}"><img src="<?= Cloudder::show($_event->public_id, ['version'=> $_event->version, 'width'=>400, 'height'=>400, 'crop'=>'fill']);?>" alt="{{$_event->event_name}}"></a>
                    </figure>
                    <div class="content-box">
                    	<h3><a href="{{route('client.event.show', ['id' => $_event->id, 'event_name' => $_event->event_name])}}">{{$_event->event_name}}</a></h3>
                        <div class="column-info">{{date('d M, Y', strtotime($_event->date_from))}} - {{date('d M, Y', strtotime($_event->date_to))}} in {{$_event->location}}</div>
                        <div class="text"><?= \Str::limit($_event->event_slug);?> </div>
                        <a href="{{route('client.event.show', ['id' => $_event->id, 'event_name' => $_event->event_name])}}" class="theme-btn btn-style-three">Read More</a>
                    </div>
                </article>
            </div>
            <?php
            $fourLatestEvents = App\Event::where('id', '!=', $_event->id)->take(4)->latest()->get();
            ?>
            
            @endforeach
        
            <!--Cause Column-->
            <div class="column default-featured-column links-column col-lg-4 col-md-6 col-sm-6 col-xs-12">
            	<article class="inner-box">
            		<div class="vertical-links-outer">
                        
                        @foreach($fourLatestEvents as $event)
                    	<div class="link-block">
                        	<div class="default inner"><figure class="image-thumb"><img src="<?= Cloudder::show($event->public_id, ['version'=> $event->version, 'width'=>400, 'height'=>400, 'crop'=>'fill']);?>" alt="{{$event->event_name}}"></figure><strong>{{$event->event_name}}</strong><span class="desc"><?= $event->event_slug;?> </span></div>
                            <a href="{{route('client.news.show', ['id' => $event->id, 'event_name' => $event->event_name])}}" class="alternate"><strong>{{$event->event_name}}</strong><span class="desc"><?= $event->event_slug;?> </span></a>
                        </div>
                        @endforeach
                        
                       
                    </div>
                </article>
            </div>
            
            @endif
            
        </div>
    </div>
</section>

<!--Blog News Section-->
<section class="blog-news-section latest-news">
	<div class="auto-container">
        <div class="sec-title text-center">
            <h2>Latest <span class="normal-font theme_color">News</span></h2>
        </div>
    	<div class="row clearfix">
        	@if($newz->count() == 0)
            <div class="column blog-news-column col-xs-12">
                <p class="alert alert-danger text-center"><i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-right: 5px;"></i>No News are added yet<i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
            </div>
            @else
            @foreach($newz as $news)
            <!--News Column-->
            <div class="column blog-news-column col-lg-4 col-md-6 col-sm-6 col-xs-12">
            	<article class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            		<figure class="image-box">
                    	<a href="#"><img src="<?= Cloudder::show($news->public_id, ['version'=> $news->version, 'width'=>400, 'height'=>400, 'crop'=>'fill']);?>" alt="{{$news->title}}"></a>
                        <div class="news-date">{{date('d', strtotime($news->created_at))}}<span class="month">{{date('M', strtotime($news->created_at))}}</span></div>
                    </figure>
                    <div class="content-box">
                    	<h3><a href="{{route('client.news.show', ['id'=>$news->id, 'news_name'=>$news->title])}}">{{$news->title}}</a></h3>
                        <div class="text"><?= \Str::limit($news->description, 50);?></div>
                        <a href="{{route('client.news.show', ['id'=>$news->id, 'news_name'=>$news->title])}}" class="theme-btn read-more">Read More</a>
                    </div>
                </article>
            </div>
            @endforeach       
            @endif     
        </div>
    </div>
</section>

@if($sponsors->count() > 0)

<section class="sponsors-section">
    <div class="auto-container">
        <div class="slider-outer">
            <ul class="sponsors-slider">
                @foreach($sponsors as $sponsor)
                <li>
                    @if(!is_null($sponsor->link))
                    <a href="{{$sponsor->link}}" target="_blank"><img src="<?= Cloudder::show($sponsor->public_id, ['version'=> $sponsor->version, 'width'=>270, 'height'=>110]);?>" alt="{{$sponsor->link}}"></a>
                    @else
                    <a href="#"><img src="<?= Cloudder::show($sponsor->public_id, ['version'=> $sponsor->version, 'width'=>270, 'height'=>110]);?>" alt="{{$sponsor->link}}"></a>
                    @endif
                </li>
                @endforeach
            </ul>
        </div>

    </div>
</section>

@endif

@endsection