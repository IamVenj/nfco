@extends('pre-login.index.index')

@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url(<?= Cloudder::show($event->public_id, ['version'=> $event->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>);">
	<div class="auto-container">
    	<div class="sec-title">
            <h1>{{$event->event_name}}</h1>
            <div class="bread-crumb"><a href="/">Home</a> / <a href="/events">Events</a> / <a href="{{route('client.event.show', ['id'=>$event->id, 'event_name'=>$event->event_name])}}" class="current">{{$event->event_name}}</a></div>
        </div>
    </div>
</section>

<div class="sidebar-page">
	<div class="auto-container">
    	<div class="row clearfix">
        	
            <div class="col-lg-12 col-md-8 col-sm-12 col-xs-12">
                
                <section class="events-section event-details no-padd-bottom no-padd-top padd-right-20">
                     
                    <div class="column default-featured-column style-two">
                        <article class="inner-box">
                            <figure class="image-box">
                                <a href="<?= Cloudder::show($event->public_id, ['version'=> $event->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>"><img src="<?= Cloudder::show($event->public_id, ['version'=> $event->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>" alt="{{$event->event_name}}"></a>
                            </figure>
                            <div class="content-box padd-top-40">
                                <div class="row detail-header clearfix">
                                    <div class="col-md-12 col-sm-12">
                                        <h3><a href="#">{{$event->event_name}}</a></h3>
                                        <div class="column-info no-margin-bottom">{{date('d M, Y', strtotime($event->date_from))}} - {{date('d M, Y', strtotime($event->date_to))}} in {{$event->location}}</div>
                                    </div>
                                </div>
                                <hr>
                                
                                <div class="text">
                                	<br>
                                    <p class="bigger-text"><?= $event->event_slug;?></p>
                                </div>
                                
                                <br><br>
                                
                                <div class="other-info">
                                	<div class="row clearfix">
                                    	<!--Info Column-->
                                        <div class="info-column column col-md-12 col-xs-12">
                                        	<h3>Event Details</h3>
                                            <ul class="info-box">
                                                <li><span class="icon fa fa-map-marker"></span><strong>Location</strong> {{$event->location}}</li>
                                                <li><span class="icon fa fa-calendar"></span><strong>Date</strong> {{date('d M y', strtotime($event->date_from))}} - {{date('d M y', strtotime($event->date_to))}}</li>
                                                <li><span class="icon fa fa-clock-o"></span><strong>Time</strong> {{$event->time}}</li>
                                            </ul>
                                        </div>
                                                                                
                                    </div>
                                </div>
                                
                            </div>
                        </article>
                    </div>
                    
                </section>
                
            </div>
            
        </div>
    </div>
</div>

@endsection