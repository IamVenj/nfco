@extends('pre-login.index.index')

@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/main-img.jpg')}});">
    <div class="auto-container">
        <div class="sec-title">
            <h1>Contact <span class="normal-font">Us</span></h1>
            <div class="bread-crumb"><a href="/">Home</a> / <a href="/contact" class="current">Contact Us</a></div>
        </div>
    </div>
</section>


<!--Default Section / Other Info-->
<section class="default-section other-info">
    <div class="auto-container">
    
        <div class="row clearfix">

            
            <!--Info Column-->
            <div class="column info-column col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <article class="inner-box">
                    <h3 class="margin-bott-20">Our Address</h3>
                    <ul class="info-box">
                        <li><strong><span class="mdi mdi-map-marker"></span>Address</strong> {{$setting->location}}</li>
                        <li><strong><span class="mdi mdi-phone"></span>Phone</strong> {{$setting->phone_number}}</li>
                        <li><strong><span class="mdi mdi-email"></span>Email</strong> {{$setting->email}}</li>
                    </ul>
                </article>
            </div>
            
            <!--Map Column-->
            <div class="column map-column col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2>Our Location on Map</h2>
                
                @if(!empty($setting->google_map_location))
                <article class="inner-box">
                    <!--Map Container-->
                    <div class="map-container">
                        <!--Map Canvas-->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31525.517948261273!2d38.76113794766865!3d9.000669423957666!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x164b8508e7628793%3A0x2eab5e5faa84a14c!2sAbyssinia%20Guest%20House!5e0!3m2!1sen!2set!4v1568818834704!5m2!1sen!2set" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        
                    </div>
                </article>
                @else
                <p class="alert alert-danger text-center"><i class="mdi mdi-information text-danger" style="margin-right: 5px;"></i>Sorry! map is not embedded yet</i></p>
                @endif
            </div>


        </div>
    </div>
</section>


<!--Contact Section-->
<section class="contact-section no-padd-top">
    <div class="auto-container">
    
        <div class="row clearfix">
        
            
            <!--Form Column-->
            <div class="column form-column col-lg-12 col-md-6 col-sm-12 col-xs-12">
                <h2>Send Message</h2>
                <!--COntact Form-->
                <div class="inner-box contact-form">
                    <form method="post" action="{{route('contact.store')}}" id="contact-form">
                        <div class="row clearfix">
                            <!--Form Group-->
                            <div class="form-group col-md-6 col-xs-12">
                                <input type="text" name="fullname" value="" placeholder="Your Name">
                            </div>
                            <!--Form Group-->
                            <div class="form-group col-md-6 col-xs-12">
                                <input type="text" name="email" value="" placeholder="Your Email">
                            </div>
                            <!--Form Group-->
                            <div class="form-group col-md-12 col-xs-12">
                                <textarea name="message" id="message-contact" placeholder="Message"></textarea>
                            </div>
                            
                            <!--Form Group-->
                            <div class="form-group col-md-12 col-xs-12">
                                <div class="text-left"><button type="submit" id="send_contact" class="theme-btn btn-style-two">Send</button></div>
                            </div>
                        </div>
                    </form>
                </div><!--COntact Form-->
                
            </div>
        
        </div>
    </div>
</section>

@endsection