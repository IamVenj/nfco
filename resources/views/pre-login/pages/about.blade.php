@extends('pre-login.index.index')

@section('content')
<!--Page Title-->
<section class="page-title" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/main-img.jpg')}});">
	<div class="auto-container">
    	<div class="sec-title">
            <h1>About Us</h1>
            <div class="bread-crumb"><a href="/">Home</a> / <a href="/about" class="current">About Us</a></div>
        </div>
    </div>
</section>


<!--Default Section-->
<section class="default-section">
	<div class="auto-container">
            
        <div class="row clearfix">
            
            <!--Column-->
            <div class="column default-text-column col-md-4 col-sm-6 col-xs-12">
            	<div class="text-block">
                	<h3>Our Mission</h3>
                    <div class="text no-margin-bottom">
                    	<p><?= $about->mission;?></p>
                    </div>
                </div>
            </div>
            
            <!--Column-->
            <div class="column default-text-column col-md-4 col-sm-6 col-xs-12">
            	<div class="text-block">
                	<h3>Our <span class="theme_color">Vision</span></h3>
                    <div class="text no-margin-bottom">
                    	<p><?= $about->vision;?></p>
                    </div>
                </div>
            </div>
            
            <!--Column-->
            <div class="column default-text-column col-md-4 col-sm-6 col-xs-12">
            	<div class="text-block">
                	<h3>Our Objective</h3>
                    <div class="text no-margin-bottom">
                    	<p><?= $about->objective;?></p>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</section>


<!--Two Column Fluid -->
<section class="two-column-fluid">
	
	<div class="outer clearfix">
        
        <article class="column left-column" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/cover-img.jpg')}});">
            
            <div class="content-box pull-right">	
                <h2><span class="normal-font theme_color">Who</span> are we?</h2>
                <div class="title-text">{{$setting->company_name}}</div>
                <div class="text"><?= $about->who_are_we; ?></div>
                <br>
                
                <div class="clearfix">
                	<div class="icon-box">
                    	<div class="icon"><span class="flaticon-shapes-1"></span></div>
                        <div class="lower-box">
                        	<h4><span class="count-text" data-stop="7845" data-speed="1500">{{App\Event::count()}}</span></h4>
                            <span class="title">Events</span>
                        </div>
                    </div>
                    
                    <div class="icon-box">
                    	<div class="icon"><span class="flaticon-tool-4"></span></div>
                        <div class="lower-box">
                        	<h4><span class="count-text" data-stop="13360" data-speed="1500">{{App\Project::count()}}</span></h4>
                            <span class="title">Projects</span>
                        </div>
                    </div>
                    
                    <div class="icon-box">
                    	<div class="icon"><span class="flaticon-favorite"></span></div>
                        <div class="lower-box">
                        	<h4><span class="count-text" data-stop="78459" data-speed="1500">{{App\Donation::count()}}</span></h4>
                            <span class="title">Donations</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </article>
        
        <article class="column right-column" style="background-image:url({{asset('images/resource/fluid-image-4.jpg')}});">
            
            <div class="content-box pull-left">	
            	<div class="outer-box">
                	<div class="quote-icon"><span class="fa fa-quote-left"></span></div>
                    <h2>Our Motto</h2>
                    
                    <!--Text Content-->
                    <div class="text-content">
                        <div class="text"><p><?= $about->motto;?></p></div>
                        
                    </div>
                    
                </div>
            </div>
            
            <div class="clearfix"></div>
        </article>
        
    </div>
    
</section>

@if($sponsors->count() > 0)

<section class="sponsors-section">
    <div class="auto-container">
        <div class="slider-outer">
            <ul class="sponsors-slider">
                @foreach($sponsors as $sponsor)
                <li>
                    @if(!is_null($sponsor->link))
                    <a href="{{$sponsor->link}}" target="_blank"><img src="<?= Cloudder::show($sponsor->public_id, ['version'=> $sponsor->version, 'width'=>270, 'height'=>110]);?>" alt="{{$sponsor->link}}"></a>
                    @else
                    <a href="#"><img src="<?= Cloudder::show($sponsor->public_id, ['version'=> $sponsor->version, 'width'=>270, 'height'=>110]);?>" alt="{{$sponsor->link}}"></a>
                    @endif
                </li>
                @endforeach
            </ul>
        </div>

    </div>
</section>

@endif

@endsection