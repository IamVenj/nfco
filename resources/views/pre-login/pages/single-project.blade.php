@extends('pre-login.index.index')

@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url(<?= Cloudder::show($project->public_id, ['version'=> $project->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>);">
	<div class="auto-container">
    	<div class="sec-title">
            <h1>{{$project->title}}</h1>
            <div class="bread-crumb"><a href="/">Home</a> / <a href="/projects">Projects</a> / <a href="{{route('client.project.show', ['id'=>$project->id, 'project_name'=>$project->title])}}" class="current">{{$project->title}}</a></div>
        </div>
    </div>
</section>


<!--Sidebar Page-->
<div class="sidebar-page">
	<div class="auto-container">
    	<div class="row clearfix">
        	
            <!--Content Side-->	
            <div class="col-lg-12 col-md-8 col-sm-12 col-xs-12">
                
                <!--Projects Section-->
                <section class="projects-section project-details no-padd-bottom no-padd-top padd-right-20">
                     
                    <div class="column default-featured-column">
                        <article class="inner-box">
                            <figure class="image-box">
                                <a href="<?= Cloudder::show($project->public_id, ['version'=> $project->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>"><img src="<?= Cloudder::show($project->public_id, ['version'=> $project->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>" alt="{{$project->title}}"></a>
                            </figure>
                            <div class="content-box padd-top-40">
                                <div class="row detail-header clearfix">
                                    <div class="col-md-12 col-sm-12">
                                        <h2>{{$project->title}}</h2>
                                    </div>
                                </div>
                                <div class="text">
                                    <br>
                                    <p class="bigger-text"><?= $project->description;?></p>
                                </div>
                                
                            </div>
                        </article>
                    </div>
                    
                </section>             
                
            </div>
            
        </div>
    </div>
</div>

@endsection