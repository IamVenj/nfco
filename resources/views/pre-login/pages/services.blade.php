@extends('pre-login.index.index')

@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/main-img.jpg')}});">
    <div class="auto-container">
        <div class="sec-title text-left">
            <h1>Our <span class="normal-font">Services</span></h1>
            <div class="bread-crumb"><a href="/">Home</a> / <a href="/services" class="current">Services</a></div>
        </div>
    </div>
</section>


<!--Main Features-->
<section class="main-features">
    <div class="auto-container">
        <div class="title-box text-center">
                <h2>Our <span class="normal-font theme_color">Services</span></h2>
        </div>
        
        <div class="row clearfix">

            @if($services->count() == 0)

            <div class="default-icon-column col-lg-12 col-md-6 col-xs-12">

                <div class="column blog-news-column col-xs-12">
                <p class="alert alert-danger text-center"><i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-right: 5px;"></i>No Service are added yet<i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
                </div>
           
            </div>

            @else

            @foreach($services as $service)
            
            <!--Default Icon Column-->
            <div class="default-icon-column col-lg-3 col-md-6 col-xs-12">
                <article class="inner-box">
                    <div class="icon-box">
                        <div class="icon"><span class="flaticon-illumination"></span></div>
                    </div>
                    <h3>{{$service->title}}</h3>
                    <div class="text"><?= $service->description;?> </div>
                </article>
            </div>
            
            @endforeach

            @endif
            
        </div>
    </div>
</section>


@endsection