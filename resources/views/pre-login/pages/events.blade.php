@extends('pre-login.index.index')

@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/main-img.jpg')}});">
	<div class="auto-container">
    	<div class="sec-title">
            <h1>Events</h1>
            <div class="bread-crumb"><a href="/">Home</a> / <a href="/events" class="current">Events</a></div>
        </div>
    </div>
</section>


<!--Events Section-->
<section class="events-section latest-events">
	<div class="auto-container">
    	
    	<div class="row clearfix">

            @if($events->count() == 0)

            <div class="column blog-news-column col-xs-12">
                <p class="alert alert-danger text-center"><i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-right: 5px;"></i>No Events are added yet<i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
            </div>

            @else

            @foreach($events as $event)
            
            <!--Featured Column-->
            <div class="column default-featured-column style-two col-lg-4 col-md-6 col-sm-6 col-xs-12">
            	<article class="inner-box">
            		<figure class="image-box">
                    	<a href="{{route('client.event.show', ['id' => $event->id, 'event_name' => $event->event_name])}}"><img src="<?= Cloudder::show($event->public_id, ['version'=> $event->version, 'width'=>500, 'height'=>500, 'crop'=>'fill']);?>" alt=""></a>
                    </figure>
                    <div class="content-box">
                    	<h3><a href="{{route('client.event.show', ['id' => $event->id, 'event_name' => $event->event_name])}}">{{$event->event_name}}</a></h3>
                        <div class="column-info">{{date('d M, Y', strtotime($event->date_from))}} - {{date('d M, Y', strtotime($event->date_to))}} in {{$event->location}}</div>
                        <div class="text"><?= \Str::limit($event->event_slug);?> </div>
                        <a href="{{route('client.event.show', ['id' => $event->id, 'event_name' => $event->event_name])}}" class="theme-btn btn-style-three">Read More</a>
                    </div>
                </article>
            </div>
            
            @endforeach

            @endif
            
        </div>
        
        <!-- Styled Pagination -->
        <div class="styled-pagination text-center padd-top-20 margin-bott-40">
            <ul>
                <li>{{$events->links()}}</li>
            </ul>
        </div>
                    
    </div>
</section>


@endsection