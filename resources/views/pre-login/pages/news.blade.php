@extends('pre-login.index.index')

@section('content')
<!--Page Title-->
<section class="page-title" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/main-img.jpg')}});">
	<div class="auto-container">
    	<div class="sec-title text-left">
            <h1>News</h1>
            <div class="bread-crumb"><a href="/">Home</a> / <a href="/news" class="current">News</a></div>
        </div>
    </div>
</section>

<!--Blog News Section-->
<section class="blog-news-section latest-news">
	<div class="auto-container">
    	
        <div class="sec-title text-center">
            <h2>Latest <span class="normal-font theme_color">News</span></h2>
        </div>
    	<div class="row clearfix">
            
            @if($newz->count() == 0)
            <div class="column blog-news-column col-xs-12">
                <p class="alert alert-danger text-center"><i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-right: 5px;"></i>No News are added yet<i class="mdi mdi-newspaper text-danger" style="font-size: 26px; margin-left: 5px;"></i></p>
            </div>
            @else
            @foreach($newz as $news)
            <!--News Column-->
            <div class="column blog-news-column col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <article class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <figure class="image-box">
                        <a href="#"><img src="<?= Cloudder::show($news->public_id, ['version'=> $news->version, 'width'=>400, 'height'=>400, 'crop'=>'fill']);?>" alt="{{$news->title}}"></a>
                        <div class="news-date">{{date('d', strtotime($news->created_at))}}<span class="month">{{date('M', strtotime($news->created_at))}}</span></div>
                    </figure>
                    <div class="content-box">
                        <h3><a href="{{route('client.news.show', ['id'=>$news->id, 'news_name'=>$news->title])}}">{{$news->title}}</a></h3>
                        <div class="text"><?= \Str::limit($news->description, 50);?></div>
                        <a href="{{route('client.news.show', ['id'=>$news->id, 'news_name'=>$news->title])}}" class="theme-btn read-more">Read More</a>
                    </div>
                </article>
            </div>
            @endforeach       
            @endif   
            
            
        </div>
    </div>
</section>

@endsection