<!-- Main Header -->
<header class="main-header">
	<div class="top-bar">
    	<div class="top-container">
        	<!--Info Outer-->
             <div class="info-outer">
             	<!--Info Box-->
                <ul class="info-box clearfix">
                	<li><span class="fa fa-envelope" style="margin-right: 5px;"></span><a href="#">{{$setting->email}}</a></li>
                	<li><span class="fa fa-phone" style="margin-right: 5px;"></span><a href="#">{{$setting->phone_number}}</a></li>
                    <li class="social-links-one">
                        @if(!is_null($setting->facebook))
                    	<a href="#" class="facebook img-circle"><span class="fa fa-facebook-f"></span></a>
                        @endif
                        @if(!is_null($setting->twitter))
                        <a href="#" class="twitter img-circle"><span class="fa fa-twitter"></span></a>
                        @endif
                        @if(!is_null($setting->google_plus))
                        <a href="#" class="google-plus img-circle"><span class="fa fa-google-plus"></span></a>
                        @endif
                        @if(!is_null($setting->linked_in))
                        <a href="#" class="linkedin img-circle"><span class="fa fa-linkedin"></span></a>
                        @endif
                        @if(!is_null($setting->pinterest))
                        <a href="#" class="linkedin img-circle"><span class="fa fa-pinterest"></span></a>
                        @endif
                    </li>
                </ul>
             </div>
        </div>
    </div>

	<div class="header-upper">
    	<div class="auto-container clearfix">

            <div class="logo">
                <a href="/"><img src="{{URL::asset('storage/uploads/logo.png')}}" alt="{{$setting->company_name}}"></a>
             </div>
             
            <div class="nav-outer clearfix">
                
                <a href="#" class="theme-btn btn-donate" data-toggle="modal" data-target="#donate-popup">Donate Now!</a>
                
                <nav class="main-menu">
                    
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation">
                        
                            <li class="@if(Request::is('/')) current @endif"><a href="/">Home</a></li>
                            <li class="@if(Request::is('about')) current @endif"><a href="/about">About</a></li>
                            <li class="@if(Request::is('services')) current @endif"><a href="/services">Services</a></li>
                            <li class="@if(Request::is('projects')) current @endif"><a href="/projects">Projects</a></li>
                            <li class="@if(Request::is('events')) current @endif"><a href="/events">Events</a></li>
                            <li class="@if(Request::is('news')) current @endif"><a href="/news">News</a></li>
                            <li class="@if(Request::is('contact')) current @endif"><a href="/contact">Contact Us</a></li>
                        </ul>
                    </div>
                </nav><!-- Main Menu End-->
                
            </div>
            
        </div>
    </div><!-- Header Top End -->
    
</header><!--End Main Header -->