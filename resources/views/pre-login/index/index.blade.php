<?php
$setting = App\CompanySetting::first();
?>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<title>{{$setting->company_name}}</title>

	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('css/revolution-slider.css')}}" rel="stylesheet">
	<link href="{{asset('css/style.css')}}" rel="stylesheet">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{csrf_token()}}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link href="{{asset('css/responsive.css')}}" rel="stylesheet">
  	<link rel="shortcut icon" href="{{URL::asset('storage/uploads/favicon.png')}}" />
  	<link rel="stylesheet" href="{{ URL::asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
	

</head>

<body>

<div class="page-wrapper">

	<div class="preloader"></div>

	@include('pre-login.index.nav')

	@yield('content')

	<div class="message-layout"></div>

	@include('pre-login.index.footer')

	<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="fa fa-long-arrow-up"></span></div>

</div>

@include('pre-login.partials.modal.donation-modal')

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> 
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/revolution.min.js')}}"></script>
	<script src="{{asset('js/jquery.fancybox.pack.js')}}"></script>
	<script src="{{asset('js/jquery.fancybox-media.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script src="{{asset('js/owl.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>
	<script src="{{asset('js/script.js')}}"></script>


</body>

</html>