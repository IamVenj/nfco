<?php
    $projects = App\Project::take(6)->latest()->get();
    $setting = App\CompanySetting::first();
    $newz = App\news::take(2)->latest()->get();
    $about = App\About::first();
?>
<!--Main Footer-->
<footer class="main-footer" style="background-image:url({{URL::asset('storage/uploads/custom-pages/all/cover-img.jpg')}});">
	
    <!--Footer Upper-->        
    <div class="footer-upper">
        <div class="auto-container">
            <div class="row clearfix">
            	
                <!--Two 4th column-->
                <div class="col-md-6 col-sm-12 col-xs-12">
                	<div class="row clearfix">
                        <div class="col-lg-7 col-sm-6 col-xs-12 column">
                            <div class="footer-widget about-widget">
                                <div class="logo"><a href="/"><img src="{{URL::asset('storage/uploads/logo.png')}}" class="img-responsive" style="height: 80px;" alt="{{$setting->company_name}}"></a></div>
                                <div class="text">
                                    <p><?= \Str::limit($about->who_are_we, 100);?></p>
                                </div>
                                
                                <ul class="contact-info">
                                	<li><span class="icon fa fa-map-marker"></span> {{$setting->location}}</li>
                                    <li><span class="icon fa fa-phone"></span> {{$setting->phone_number}}</li>
                                    <li><span class="icon fa fa-envelope-o"></span> {{$setting->email}}</li>
                                </ul>
                                
                                <div class="social-links-two clearfix">
                                    @if(!is_null($setting->facebook))
                                	<a href="{{$setting->facebook}}" class="facebook img-circle"><span class="fa fa-facebook-f"></span></a>
                                    @endif
                                    @if(!is_null($setting->twitter))
                                    <a href="{{$setting->twitter}}" class="twitter img-circle"><span class="fa fa-twitter"></span></a>
                                    @endif
                                    @if(!is_null($setting->google_plus))
                                    <a href="{{$setting->google_plus}}" class="google-plus img-circle"><span class="fa fa-google-plus"></span></a>
                                    @endif
                                    @if(!is_null($setting->pinterest))
                                    <a href="{{$setting->pinterest}}" class="linkedin img-circle"><span class="fa fa-pinterest-p"></span></a>
                                    @endif
                                    @if(!is_null($setting->linked_in))
                                    <a href="{{$setting->linked_in}}" class="linkedin img-circle"><span class="fa fa-linkedin"></span></a>
                                    @endif
                                </div>
                                
                            </div>
                        </div>
                        
                        <!--Footer Column-->
                        <div class="col-lg-5 col-sm-6 col-xs-12 column">
                            <h2>Our Project</h2>
                            <div class="footer-widget links-widget">
                                <ul>
                                    @if($projects->count() == 0)
                                    <li><p class="alert alert-danger">No Projects are added</p></li>
                                    @endif
                                    @foreach($projects as $project)
                                    <li><a href="{{route('client.project.show', ['id'=>$project->id, 'project_name'=>$project->title])}}">{{$project->title}}</a></li>
                                    @endforeach
                                </ul>
    
                            </div>
                        </div>
                	</div>
                </div><!--Two 4th column End-->
                
                <!--Two 4th column-->
                <div class="col-md-6 col-sm-12 col-xs-12">
                	<div class="row clearfix">
                		<!--Footer Column-->
                    	<div class="col-lg-7 col-sm-6 col-xs-12 column">
                        	<div class="footer-widget news-widget">
                            	<h2>Latest News</h2>	
                                @if($newz->count() == 0)
                                    <p class="alert alert-danger">No News are added</p>
                                @endif
                                @foreach($newz as $news)
                                <!--News Post-->
                                <div class="news-post">
                                	<div class="icon"></div>
                                    <div class="news-content">
                                        <figure class="image-thumb"><img src="<?= Cloudder::show($news->public_id, ['version'=> $news->version, 'width'=>400, 'height'=>400, 'crop'=>'fill']);?>" alt=""></figure>
                                        <a href="{{route('client.news.show', ['id' => $news->id, 'news_name' => $news->title])}}">
                                        {{$news->title}}</a>
                                    </div>
                                    <div class="time">{{date('M d, Y', strtotime($news->created_at))}}</div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        
                        <!--Footer Column-->
                        <div class="col-lg-5 col-sm-6 col-xs-12 column">
                            <div class="footer-widget links-widget">
                            	<h2>Quick Links</h2>
                                <ul>
                                    <li><a href="/" style="text-transform: uppercase;">Home</a></li>
                                    <li><a href="/about" style="text-transform: uppercase;">About</a></li>
                                    <li><a href="/services" style="text-transform: uppercase;">Services</a></li>
                                    <li><a href="/projects" style="text-transform: uppercase;">Projects</a></li>
                                    <li><a href="/events" style="text-transform: uppercase;">Events</a></li>
                                    <li><a href="/news" style="text-transform: uppercase;">News</a></li>
                                    <li><a href="/contact" style="text-transform: uppercase;">Contact Us</a></li>
                                </ul>
    
                            </div>
                        </div>
                	</div>
                </div><!--Two 4th column End-->
                
            </div>
            
        </div>
    </div>
    
    <!--Footer Bottom-->
	<div class="footer-bottom">
        <div class="auto-container clearfix">
            <!--Copyright-->
            <div class="copyright text-center">Copyright <?date('Y')?> &copy; {{$setting->company_name}} All rights reserved. 
        </div>
    </div>
    
</footer>