<div class="modal fade pop-box" id="donate-popup" tabindex="-1" role="dialog" aria-labelledby="donate-popup" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <section class="donation-section">
                <div class="donation-form-outer">
                    <form method="post" action="{{route('donate.store')}}" id="donate-form">
                        
                        <div class="form-portlet">
                            <h3>How Much Would you like to Donate?</h3>
                            
                            <div class="row clearfix">                               
                            
                                <div class="form-group other-amount col-lg-12 col-md-8 col-xs-12 padd-top-10">     
                                    <input type="text" name="amount" value="" placeholder="Amount in birr eg, 2000 BR">
                                </div>
                                
                            </div>
                        </div>
                        
                        <br>
                        
                        <!--Form Portlet-->
                        <div class="form-portlet">
                            <h4>Make a Promise</h4>
                            
                            <div class="row clearfix">
                                
                                <div class="form-group col-lg-6 col-md-6 col-xs-12">
                                    <div class="field-label">First Name <span class="required">*</span></div>
                                    <input type="text" name="firstname" value="" placeholder="First Name" required>
                                </div>
                                
                                <div class="form-group col-lg-6 col-md-6 col-xs-12">
                                    <div class="field-label">Last Name <span class="required">*</span></div>
                                    <input type="text" name="lastname" value="" placeholder="Last Name" required>
                                </div>
                                
                                <div class="form-group col-lg-6 col-md-6 col-xs-12">
                                    <div class="field-label">Email <span class="required">*</span></div>
                                    <input type="email" name="_email" value="" placeholder="Email" required>
                                </div>
                                
                                <div class="form-group col-lg-6 col-md-6 col-xs-12">
                                    <div class="field-label">Phone <span class="required">*</span></div>
                                    <input type="text" name="phone" value="" placeholder="Phone" required>
                                </div>
                                
                                <div class="form-group col-lg-6 col-md-6 col-xs-12">
                                    <div class="field-label">Address 1 <span class="required">*</span></div>
                                    <input type="text" name="address1" value="" placeholder="Address 1" required>
                                </div>
                                
                                <div class="form-group col-lg-6 col-md-6 col-xs-12">
                                    <div class="field-label">Address 2</div>
                                    <input type="text" name="address2" value="" placeholder="Address 2">
                                </div>
                                
                            </div>
                        </div>
                        
                        <br>
                                                
                        <div class="text-left">
                            <button type="submit" class="theme-btn btn-style-two" id="promise-btn">I Promise</button>
                        </div>
                        
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
