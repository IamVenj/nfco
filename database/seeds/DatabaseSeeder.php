<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->delete();

        factory(App\User::class)->create([

            'name' => 'admin',

            'email' => 'admin@admin.com',

            'password' => bcrypt('_nfcoadmin'),

        ]);

        DB::table('abouts')->delete();

        factory(App\About::class)->create([

            'mission' => 'To improve the awareness of the community towards the socio economic situations of green plants in addis ababa city and bishoftu city, green spaces around residential areas in the local and national overall development.<br> To sensitize Addis Ababa dwellers and Ethiopians as a whole in particular environmental and estabilish tree nurseries in various parts of the country that will enable the public to acquire free seedling for fruits, amenity and agro-forestry.',
            'vision' => 'To see: an environmentally friendly, ecologically stable and economically flourishing bishoftu and Addis ababa for the benefit of the people of Ethiopia. <br> To see: a culture of environmental protection amongst the citizens and revitalize Ethiopia to a state where the populace has sufficient knowledge to mitigate and adapt to climate change effects in time.',
            'objective' => 'To see: an environmentally friendly, ecologically stable and economically flourishing bishoftu and Addis ababa for the benefit of the people of Ethiopia. <br> To see: a culture of environmental protection amongst the citizens and revitalize Ethiopia to a state where the populace has sufficient knowledge to mitigate and adapt to climate change effects in time.',
            'who_are_we' => 'To see: an environmentally friendly, ecologically stable and economically flourishing bishoftu and Addis ababa for the benefit of the people of Ethiopia. <br> To see: a culture of environmental protection amongst the citizens and revitalize Ethiopia to a state where the populace has sufficient knowledge to mitigate and adapt to climate change effects in time.',

        ]);


        DB::table('company_settings')->delete();

        factory(App\CompanySetting::class)->create([

            'company_name' => 'NFCO',
            'location' => '291 South 21th Street, Suite 721 New York NY 10016',
            'phone_number' => '+ 1235 2355 98',
            'email' => 'info@yoursite.com',

        ]);
    }
}
